package com.malencur.file.properties.factory

import com.malencur.file.FileFactory
import com.malencur.file.properties.property.*
import java.time.LocalDateTime

/**
 * Constructs properties from external file
 *
 * @since 1.0.0
 * @author alog
 */
open class ExternalPropertiesFactory(fileName: String, rootDir: String, subDir: String? = null) :
        PropertiesFactory(
                ExternalPropertiesLazyProperty(FileFactory.getDestinationFilePath(fileName, rootDir, subDir)),
                fileName,
                rootDir,
                subDir
        ) {

    override fun saveDocument() {
        propDelegate.persist()
    }

    fun stringPersistedProperty(
            selector: String? = null,
            default: String? = null,
            persisted: Boolean = false
    ) = StringPersistedProperty(this, selector, default, persisted)

    fun stringListPersistedProperty(
            selector: String? = null,
            delimiter: String? = null,
            default: List<String>? = null,
            persisted: Boolean = false
    ) = StringListPersistedProperty(this, selector, delimiter, default, persisted)

    fun intPersistedProperty(
            selector: String? = null,
            default: Int? = null,
            persisted: Boolean = false
    ) = IntegerPersistedProperty(this, selector, default, persisted)

    fun intListPersistedProperty(
            selector: String? = null,
            delimiter: String? = null,
            default: List<Int>? = null,
            persisted: Boolean = false
    ) = IntegerListPersistedProperty(this, selector, delimiter, default, persisted)

    fun booleanPersistedProperty(
            selector: String? = null,
            default: Boolean? = null,
            persisted: Boolean = false
    ) = BooleanPersistedProperty(this, selector, default, persisted)

    fun localDateTimePersistedProperty(
            selector: String? = null,
            default: LocalDateTime? = null,
            persisted: Boolean = false
    ) = LocalDateTimePersistedProperty(this, selector, default, persisted)

    fun localDateTimeListPersistedProperty(
            selector: String? = null,
            delimiter: String? = null,
            default: List<LocalDateTime>? = null,
            persisted: Boolean = false
    ) = LocalDateTimeListPersistedProperty(this, selector, delimiter, default, persisted)
}