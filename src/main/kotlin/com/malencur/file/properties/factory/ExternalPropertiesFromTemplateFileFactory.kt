package com.malencur.file.properties.factory

import java.io.IOException

/**
 * Creates [java.util.Properties] instance from a file.
 *
 * If the file not found - generates default one from project resources.
 *
 * @since 1.0.0
 * @author alog
 */
abstract class ExternalPropertiesFromTemplateFileFactory(fileName: String, rootDir: String, subDir: String? = null) :
        ExternalPropertiesFactory(fileName, rootDir, subDir) {

    init {
        if (!fileExists) {
            if (!generateDefaultFileFromResources()) {
                throw IOException("Cannot create $fileName file.")
            }
        }
    }
}