package com.malencur.file.properties.factory

import com.malencur.file.FileFactory
import com.malencur.file.properties.property.*
import java.time.LocalDateTime

abstract class PropertiesFactory(protected val propDelegate: PropertiesLazyProperty, fileName: String, rootDir: String, subDir: String? = null) :
        FileFactory(fileName, rootDir, subDir) {

    val document by propDelegate

    abstract fun saveDocument()

    fun stringProperty(selector: String? = null, default: String? = null) = StringProperty(this, selector, default)

    fun stringListProperty(
            selector: String? = null,
            delimiter: String? = null,
            default: List<String>? = null
    ) = StringListProperty(this, selector, delimiter, default)

    fun intProperty(selector: String? = null, default: Int? = null) = IntegerProperty(this, selector, default)

    fun intListProperty(
            selector: String? = null,
            delimiter: String? = null,
            default: List<Int>? = null
    ) = IntegerListProperty(this, selector, delimiter, default)

    fun booleanProperty(selector: String? = null, default: Boolean? = null) = BooleanProperty(this, selector, default)

    fun localDateTimeProperty(
            selector: String? = null,
            default: LocalDateTime? = null
    ) = LocalDateTimeProperty(this, selector, default)

    fun localDateTimeListProperty(
            selector: String? = null,
            delimiter: String? = null,
            default: List<LocalDateTime>? = null
    ) = LocalDateTimeListProperty(this, selector, delimiter, default)
}