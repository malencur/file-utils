package com.malencur.file.properties.factory

import com.malencur.file.properties.property.InternalPropertiesLazyProperty

/**
 * Constructs properties from internal resource file (read only)
 *
 * @since 1.0.0
 * @author alog
 */
open class InternalPropertiesFactory(fileName: String, classLoader: ClassLoader? = null) : PropertiesFactory(
        InternalPropertiesLazyProperty(fileName, classLoader),
        fileName,
        NONE_EXISTING_DIRECTORY
) {
    override fun saveDocument() {}

    companion object {
        private const val NONE_EXISTING_DIRECTORY = "dummy"
    }
}