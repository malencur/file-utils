package com.malencur.file.properties.property

import java.io.FileNotFoundException
import java.util.*
import kotlin.reflect.KProperty

/**
 * Provides common properties resolving logic.
 *
 * @since 1.0.0
 * @author alog
 */
abstract class PropertiesLazyProperty(protected val filePath: String, protected val classLoader: ClassLoader? = null) {
    private var initialised = false

    protected var document: Properties? = null

    operator fun getValue(thisRef: Any?, property: KProperty<*>): Properties {
        if (initialised) return document!!
        val newDocument = construct()
        document = newDocument
        return newDocument
    }

    @Throws(FileNotFoundException::class)
    protected abstract fun construct(): Properties

    /**
     * Persists document back into the file
     */
    abstract fun persist()
}