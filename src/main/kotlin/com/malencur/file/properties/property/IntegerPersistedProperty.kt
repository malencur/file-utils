package com.malencur.file.properties.property

import com.malencur.file.properties.factory.ExternalPropertiesFactory
import kotlin.reflect.KProperty

/**
 * Delegate for integer property representation.
 *
 * @since 1.0.0
 * @author alog
 */
class IntegerPersistedProperty(
        factory: ExternalPropertiesFactory,
        selector: String? = null,
        default: Int? = null,
        private val persisted: Boolean = false
) : IntegerProperty(factory, selector, default) {

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Int) {
        factory.document.setProperty(getKey(property), value.toString())
        if (persisted) factory.saveDocument()
    }
}