package com.malencur.file.properties.property

import com.malencur.file.properties.factory.PropertiesFactory
import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeParseException
import kotlin.reflect.KProperty

/**
 * Delegate for list of dates property representation.
 *
 * @since 1.0.0
 * @author alog
 */
open class LocalDateTimeListProperty(
        factory: PropertiesFactory,
        selector: String? = null,
        delimiter: String? = null,
        protected val default: List<LocalDateTime>? = null
) : PropertyListResolver(factory, selector, delimiter) {

    /**
     * @throws DateTimeParseException if the string value is not a valid representation of a [LocalDateTime]
     * @throws IOException if the property not found and no default provided
     */
    @Throws(DateTimeParseException::class, IOException::class)
    operator fun getValue(thisRef: Any?, property: KProperty<*>): List<LocalDateTime> =
            getListPropertyValue(getKey(property), default) { LocalDateTime.parse(it.trim()) }
}