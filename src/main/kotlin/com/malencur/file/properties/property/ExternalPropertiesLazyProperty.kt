package com.malencur.file.properties.property

import org.slf4j.LoggerFactory
import java.io.*
import java.time.LocalDateTime
import java.util.*

/**
 * Delegate for lazy properties creation from an external file.
 *
 * @since 1.0.0
 * @author alog
 */
class ExternalPropertiesLazyProperty(filePath: String) : PropertiesLazyProperty(filePath) {

    @Throws(FileNotFoundException::class)
    override fun construct(): Properties {

        val properties = Properties()

        //bug: we have to do it cause cyrillic will not work on windows with standard approach
        InputStreamReader(FileInputStream(filePath), "UTF-8").use {
            properties.load(it)
        }

        return properties
    }

    /**
     * Persists document back into the file
     */
    override fun persist() {
        if (document == null) return

        OutputStreamWriter(FileOutputStream(filePath), "UTF-8").use {
            try {
                document!!.store(it, "saved: ${LocalDateTime.now()}")
            } catch (ex: Exception) {
                log.error(ex.message, ex.cause)
            }
        }
    }

    companion object {
        private val log = LoggerFactory.getLogger(ExternalPropertiesLazyProperty::class.java)
    }
}