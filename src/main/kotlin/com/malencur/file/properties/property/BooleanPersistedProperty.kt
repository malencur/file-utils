package com.malencur.file.properties.property

import com.malencur.file.properties.factory.ExternalPropertiesFactory
import kotlin.reflect.KProperty

/**
 * Delegate for boolean property representation.
 *
 * @since 1.0.0
 * @author alog
 */
class BooleanPersistedProperty(
        factory: ExternalPropertiesFactory,
        selector: String? = null,
        default: Boolean? = null,
        private val persisted: Boolean = false
) : BooleanProperty(factory, selector, default) {

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Boolean) {
        factory.document.setProperty(getKey(property), value.toString())
        if (persisted) factory.saveDocument()
    }
}