package com.malencur.file.properties.property

import com.malencur.file.properties.factory.PropertiesFactory
import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeParseException
import kotlin.reflect.KProperty

/**
 * Delegate for date property representation.
 *
 * @since 1.0.0
 * @author alog
 */
open class LocalDateTimeProperty(
        factory: PropertiesFactory,
        selector: String? = null,
        protected val default: LocalDateTime? = null
) : PropertyResolver(factory, selector) {


    /**
     * @throws DateTimeParseException if the string value is not a valid representation of a [LocalDateTime]
     * @throws IOException if the property not found and no default provided
     */
    @Throws(DateTimeParseException::class, IOException::class)
    operator fun getValue(thisRef: Any?, property: KProperty<*>): LocalDateTime =
            getPropertyValue(getKey(property), default) { LocalDateTime.parse(it) }
}