package com.malencur.file.properties.property

import com.malencur.file.properties.factory.PropertiesFactory
import java.io.IOException

/**
 * Defines property getters.
 *
 * @since 1.0.0
 * @author alog
 */
open class PropertyListResolver(
        factory: PropertiesFactory,
        selector: String? = null,
        private val delimiter: String? = null
) : PropertyResolver(factory, selector) {

    protected fun getDelimiter() = if (delimiter.isNullOrBlank()) "," else delimiter

    /**
     * Transforms a coma separated sting items into a list of objects.
     *
     * @throws IOException if the property not found and no defaults provided
     */
    protected fun <T> getListPropertyValue(
            key: String, default: List<T>? = null,
            getFromString: (stringValue: String) -> T
    ): List<T> {
        val stringWithDelimiters: String? = factory.document.getProperty(key)
        if (stringWithDelimiters == null || stringWithDelimiters.isBlank()) {
            if (default != null) return default
            throw IOException("Cannot find $key property")
        }
        return stringWithDelimiters.split(getDelimiter()).map { getFromString(it) }
    }
}