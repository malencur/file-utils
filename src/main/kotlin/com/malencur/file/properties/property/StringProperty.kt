package com.malencur.file.properties.property

import com.malencur.file.properties.factory.PropertiesFactory
import java.io.IOException
import kotlin.reflect.KProperty

/**
 * Delegate for string property representation.
 *
 * @since 1.0.0
 * @author alog
 */
open class StringProperty(
        factory: PropertiesFactory,
        selector: String? = null,
        protected val default: String? = null
) : PropertyResolver(factory, selector) {

    /**
     * @throws IOException if the property not found and no default provided
     */
    @Throws(IOException::class)
    operator fun getValue(thisRef: Any?, property: KProperty<*>) =
            getPropertyValue(getKey(property), default) { it }
}