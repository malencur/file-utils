package com.malencur.file.properties.property

import com.malencur.file.properties.factory.PropertiesFactory
import java.io.IOException
import kotlin.reflect.KProperty

/**
 * Delegate for boolean property representation.
 *
 * @since 1.0.0
 * @author alog
 */
open class BooleanProperty(
        factory: PropertiesFactory,
        selector: String? = null,
        protected val default: Boolean? = null
) : PropertyResolver(factory, selector) {

    /**
     * Parses the string argument as a boolean.
     *
     * The boolean returned represents the value TRUE if the string argument is not null and is equal,
     * ignoring case, to the string "true". Otherwise the returned boolean will be always FALSE.
     *
     * @see [java.lang.Boolean.parseBoolean]
     *
     * @throws IOException if the property not found and no default provided
     */
    @Throws(IOException::class)
    operator fun getValue(thisRef: Any?, property: KProperty<*>) =
            getPropertyValue(getKey(property), default) { it.toBoolean() }
}