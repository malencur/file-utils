package com.malencur.file.properties.property

import com.malencur.file.properties.factory.ExternalPropertiesFactory
import kotlin.reflect.KProperty

/**
 * Delegate for list of integers property representation.
 *
 * @since 1.0.0
 * @author alog
 */
class IntegerListPersistedProperty(
        factory: ExternalPropertiesFactory,
        selector: String? = null,
        delimiter: String? = null,
        default: List<Int>? = null,
        private val persisted: Boolean = false
) : IntegerListProperty(factory, selector, delimiter, default) {

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: List<Int>) {
        factory.document.setProperty(getKey(property), value.joinToString(getDelimiter()) { it.toString() })
        if (persisted) factory.saveDocument()
    }
}