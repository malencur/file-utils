package com.malencur.file.properties.property

import com.malencur.file.properties.PropertyName
import com.malencur.file.properties.factory.PropertiesFactory
import java.io.IOException
import kotlin.reflect.KProperty
import kotlin.reflect.full.findAnnotation

/**
 * Defines property getters.
 *
 * @since 1.0.0
 * @author alog
 */
open class PropertyResolver(val factory: PropertiesFactory, val selector: String? = null) {

    protected fun getKey(property: KProperty<*>) =
            if (selector == null) getPropertyName(property) else "$selector.${getPropertyName(property)}"

    /**
     * @throws IOException if the property not found and no defaults provided
     */
    protected fun <T> getPropertyValue(key: String, default: T? = null, getFromString: (stringValue: String) -> T): T {
        val stringValue: String? = factory.document.getProperty(key)
        if (stringValue == null || stringValue.isBlank()) {
            if (default != null) return default
            throw IOException("Cannot find $key property")
        }
        return getFromString(stringValue)
    }

    private fun getPropertyName(property: KProperty<*>) = property.findAnnotation<PropertyName>()?.name
            ?: property.name
}