package com.malencur.file.properties.property

import com.malencur.file.properties.factory.ExternalPropertiesFactory
import java.time.LocalDateTime
import kotlin.reflect.KProperty

/**
 * Delegate for list of dates property representation.
 *
 * @since 1.0.0
 * @author alog
 */
class LocalDateTimeListPersistedProperty(
        factory: ExternalPropertiesFactory,
        selector: String? = null,
        delimiter: String? = null,
        default: List<LocalDateTime>? = null,
        private val persisted: Boolean = false
) : LocalDateTimeListProperty(factory, selector, delimiter, default) {

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: List<LocalDateTime>) {
        factory.document.setProperty(getKey(property), value.joinToString(getDelimiter()) { it.toString() })
        if (persisted) factory.saveDocument()
    }
}