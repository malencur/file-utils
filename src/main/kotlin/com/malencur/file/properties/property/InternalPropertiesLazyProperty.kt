package com.malencur.file.properties.property

import com.malencur.file.inputStreamFromResource
import java.io.FileNotFoundException
import java.io.InputStreamReader
import java.util.*

/**
 * Delegate for lazy properties creation from an internal resource.
 *
 * @since 1.0.0
 * @author alog
 */
class InternalPropertiesLazyProperty(filePath: String, classLoader: ClassLoader? = null) : PropertiesLazyProperty(filePath, classLoader) {

    @Throws(FileNotFoundException::class)
    override fun construct(): Properties {

        val properties = Properties()

        val inputStream = if (classLoader != null) {
            inputStreamFromResource(classLoader, filePath)
        } else {
            this.inputStreamFromResource(filePath)
        }

        //bug: we have to do it cause cyrillic will not work on windows with standard approach
        InputStreamReader(inputStream, "UTF-8").use {
            properties.load(it)
        }

        return properties
    }

    override fun persist() {
        throw NotImplementedError("Internal properties cannot be overridden.")
    }
}