package com.malencur.file.properties.property

import com.malencur.file.properties.factory.ExternalPropertiesFactory
import kotlin.reflect.KProperty

/**
 * Delegate for list of strings property representation.
 *
 * @since 1.0.0
 * @author alog
 */
class StringListPersistedProperty(
        factory: ExternalPropertiesFactory,
        selector: String? = null,
        delimiter: String? = null,
        default: List<String>? = null,
        private val persisted: Boolean = false
) : StringListProperty(factory, selector, delimiter, default) {

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: List<String>) {
        factory.document.setProperty(getKey(property), value.joinToString(getDelimiter()) { it })
        if (persisted) factory.saveDocument()
    }
}