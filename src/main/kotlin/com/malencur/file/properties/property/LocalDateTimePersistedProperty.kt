package com.malencur.file.properties.property

import com.malencur.file.properties.factory.ExternalPropertiesFactory
import java.time.LocalDateTime
import kotlin.reflect.KProperty

/**
 * Delegate for date property representation.
 *
 * @since 1.0.0
 * @author alog
 */
class LocalDateTimePersistedProperty(
        factory: ExternalPropertiesFactory,
        selector: String? = null,
        default: LocalDateTime? = null,
        private val persisted: Boolean = false
) : LocalDateTimeProperty(factory, selector, default) {

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: LocalDateTime) {
        factory.document.setProperty(getKey(property), value.toString())
        if (persisted) factory.saveDocument()
    }
}