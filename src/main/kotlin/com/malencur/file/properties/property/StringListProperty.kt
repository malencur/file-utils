package com.malencur.file.properties.property

import com.malencur.file.properties.factory.PropertiesFactory
import java.io.IOException
import kotlin.reflect.KProperty

/**
 * Delegate for list of strings property representation.
 *
 * @since 1.0.0
 * @author alog
 */
open class StringListProperty(
        factory: PropertiesFactory,
        selector: String? = null,
        delimiter: String? = null,
        protected val default: List<String>? = null
) : PropertyListResolver(factory, selector, delimiter) {

    /**
     * @throws IOException if the property not found and no default provided
     */
    @Throws(IOException::class)
    operator fun getValue(thisRef: Any?, property: KProperty<*>) =
            getListPropertyValue(getKey(property), default) { it.trim() }
}