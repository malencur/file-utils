package com.malencur.file.properties.property

import com.malencur.file.properties.factory.ExternalPropertiesFactory
import kotlin.reflect.KProperty

/**
 * Delegate for string property representation.
 *
 * @since 1.0.0
 * @author alog
 */
class StringPersistedProperty(
        factory: ExternalPropertiesFactory,
        selector: String? = null,
        default: String? = null,
        private val persisted: Boolean = false
) : StringProperty(factory, selector, default) {

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
        factory.document.setProperty(getKey(property), value)
        if (persisted) factory.saveDocument()
    }
}