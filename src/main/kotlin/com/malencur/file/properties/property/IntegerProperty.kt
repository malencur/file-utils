package com.malencur.file.properties.property

import com.malencur.file.properties.factory.PropertiesFactory
import java.io.IOException
import kotlin.reflect.KProperty

/**
 * Delegate for integer property representation.
 *
 * @since 1.0.0
 * @author alog
 */
open class IntegerProperty(
        factory: PropertiesFactory,
        selector: String? = null,
        protected val default: Int? = null
) : PropertyResolver(factory, selector) {

    /**
     * @throws NumberFormatException if the string value is not a valid representation of a number
     * @throws IOException if the property not found and no default provided
     */
    @Throws(NumberFormatException::class, IOException::class)
    operator fun getValue(thisRef: Any?, property: KProperty<*>) =
            getPropertyValue(getKey(property), default) { it.toInt() }
}