package com.malencur.file.properties.property

import com.malencur.file.properties.factory.PropertiesFactory
import java.io.IOException
import kotlin.reflect.KProperty

/**
 * Delegate for list of integers property representation.
 *
 * @since 1.0.0
 * @author alog
 */
open class IntegerListProperty(
        factory: PropertiesFactory,
        selector: String? = null,
        delimiter: String? = null,
        protected val default: List<Int>? = null
) : PropertyListResolver(factory, selector, delimiter) {

    /**
     * @throws NumberFormatException if the string value is not a valid representation of a number
     * @throws IOException if the property not found and no default provided
     */
    @Throws(NumberFormatException::class, IOException::class)
    operator fun getValue(thisRef: Any?, property: KProperty<*>) =
            getListPropertyValue(getKey(property), default) { it.trim().toInt() }
}