package com.malencur.file.properties

/**
 * Specifies the property name that has to be associated with a kotlin property.
 *
 * Use this annotation for properties whose names are different from their .properties representation.
 */
@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class PropertyName(val name: String)