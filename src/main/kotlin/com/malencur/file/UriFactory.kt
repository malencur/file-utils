package com.malencur.file

import java.io.IOException
import java.net.URI
import java.net.URL

/**
 * Creates [URI] for a project resource file or throws [IOException]
 * @return [URI] for requested resource or throws [IOException]
 */
@Throws(IOException::class)
fun Any.uriForResource(path: String) = uriForResource(this.javaClass.classLoader, path)

/**
 * Creates [URI] for a resource that was loaded by supplied [ClassLoader]
 * @return [URI] for requested resource or throws [IOException]
 */
@Throws(IOException::class)
fun uriForResource(classloader: ClassLoader, path: String): URI = urlForResource(classloader, path).toURI()

/**
 * Creates [URL] for a project resource file or throws [IOException]
 * @return [URL] for requested resource or throws [IOException]
 */
@Throws(IOException::class)
fun Any.urlForResource(path: String): URL = urlForResource(this.javaClass.classLoader, path)

/**
 * Creates [URL] for a resource that was loaded by supplied [ClassLoader]
 * @return [URL] for requested resource or throws [IOException]
 */
@Throws(IOException::class)
fun urlForResource(classloader: ClassLoader, path: String): URL =
        classloader.getResource(path) ?: throw IOException("Cannot find resource [$path]")