package com.malencur.file.resources

import com.malencur.file.resources.property.BundleStringProperty
import java.util.*

open class ResourceBundleFactory(path: String, locale: Locale) {

    constructor(path: String, language: String, country: String) : this(path, Locale(language, country))

    val bundle by lazy { ResourceBundle.getBundle(path, locale) }

    fun stringProperty(selector: String? = null, default: String? = null) =
            BundleStringProperty(bundle, selector, default)
}