package com.malencur.file.resources.property

import com.malencur.file.resources.BundlePropertyName
import java.io.IOException
import java.util.*
import kotlin.reflect.KProperty
import kotlin.reflect.full.findAnnotation

/**
 * Defines resourceBundle property getters.
 *
 * @since 1.0.0
 * @author alog
 */
open class BundlePropertyResolver(private val bundle: ResourceBundle, val selector: String? = null) {

    protected fun getKey(property: KProperty<*>) = if (selector == null) getPropertyName(property) else "$selector.${getPropertyName(property)}"

    /**
     * @throws IOException if the property not found and no defaults provided
     */
    protected fun <T> getPropertyValue(key: String, default: T? = null, getFromString: (stringValue: String) -> T): T {
        val stringValue: String? = bundle.getString(key)
        if (stringValue == null || stringValue.isBlank()) {
            if (default != null) return default
            throw IOException("Cannot find $key property")
        }
        return getFromString(stringValue)
    }

    private fun getPropertyName(property: KProperty<*>) = property.findAnnotation<BundlePropertyName>()?.name
            ?: property.name
}