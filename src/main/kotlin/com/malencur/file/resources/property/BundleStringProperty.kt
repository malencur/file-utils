package com.malencur.file.resources.property

import java.io.IOException
import java.util.*
import kotlin.reflect.KProperty

/**
 * Delegate for resourceBundle string property representation.
 *
 * @since 1.0.0
 * @author alog
 */
open class BundleStringProperty(
        bundle: ResourceBundle,
        selector: String? = null,
        protected val default: String? = null
) : BundlePropertyResolver(bundle, selector) {

    /**
     * @throws IOException if the property not found and no default provided
     */
    @Throws(IOException::class)
    operator fun getValue(thisRef: Any?, property: KProperty<*>) =
            getPropertyValue(getKey(property), default) { it }
}