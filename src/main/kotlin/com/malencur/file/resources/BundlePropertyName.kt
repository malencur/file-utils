package com.malencur.file.resources

/**
 * Specifies the bundle property name that has to be associated with a kotlin property.
 *
 * Use this annotation for properties whose names are different from their bundle.properties representation.
 */
@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class BundlePropertyName(val name: String)