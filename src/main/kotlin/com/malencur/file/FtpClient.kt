package com.malencur.file

import kotlinx.coroutines.*
import org.apache.commons.net.ProtocolCommandEvent
import org.apache.commons.net.ProtocolCommandListener
import org.apache.commons.net.ftp.FTPClient
import org.apache.commons.net.ftp.FTPConnectionClosedException
import org.apache.commons.net.ftp.FTPReply
import org.slf4j.LoggerFactory
import java.io.*
import java.net.SocketException
import java.net.UnknownHostException
import kotlin.coroutines.CoroutineContext

open class FtpClient(
        private val server: String,
        private val port: Int,
        private val user: String,
        private val password: String,
        private val serverInPassiveMode: Boolean = true
) : ProtocolCommandListener, CoroutineScope {

    protected val job = Job()

    override val coroutineContext: CoroutineContext
        get() = job

    protected val exceptionHandler = CoroutineExceptionHandler { _, exception ->
        log.error("${exception.message} [suppressed exceptions: ${exception.suppressed?.contentToString()}]", exception)
    }

    override fun protocolCommandSent(event: ProtocolCommandEvent?) = logFtpMessage(event)

    override fun protocolReplyReceived(event: ProtocolCommandEvent?) = logFtpMessage(event)

    private fun logFtpMessage(event: ProtocolCommandEvent?) {
        event?.let { log.debug(it.message, it) }
    }

    protected val ftp = FTPClient()

    @Volatile
    private var noConnectionException = true

    val connected: Boolean
        get() = ftp.isConnected && noConnectionException

    val available: Boolean
        get() = ftp.isAvailable && noConnectionException

    @Throws(IOException::class, SocketException::class, UnknownHostException::class)
    fun load() =
            ftp.also {
                it.addProtocolCommandListener(this)
            }.run {
                log.info("Trying to connect to server = [$server], port = [$port]")
                connect(server, port)
                if (!FTPReply.isPositiveCompletion(replyCode)) {
                    disconnect()
                    throw IOException("Cannot connect to server, reply = [$replyString]")
                }
                log.info("Trying to login user = [$user]")
                login(user, password).also {
                    if (serverInPassiveMode) ftp.enterLocalPassiveMode()
                }
            }

    @Throws(IOException::class)
    fun close() {
        ftp.disconnect()
    }

    /**
     * Tries to reconnect to the server every 2s. Starts in new Thread and returns immediately.
     */
    open fun reconnect() {
        launch(exceptionHandler) {
            var count = 0
            while (!connected) {
                delay(2000)
                connect(++count)
            }
        }
    }

    protected suspend fun connect(count: Int) {
        withContext(Dispatchers.IO) {
            try {
                log.info("$count attempt to connect to server = [$server], port = [$port], user = [$user]")
                with(ftp) {
                    connect(server, port)
                    if (!FTPReply.isPositiveCompletion(replyCode)) {
                        disconnect()
                        throw IOException("Cannot connect to server, reply = [$replyString]")
                    }
                    if (login(user, password)) {
                        noConnectionException = true
                    }
                }
            } catch (e: SocketException) {
                log.error("$count attempt to connect", e)
            } catch (e: IOException) {
                log.error("$count attempt to connect", e)
            }
        }
    }

    @Throws(IOException::class, FTPConnectionClosedException::class)
    fun fileExists(path: String) = connectionExceptionWrapper {
        val files = ftp.listFiles(path)
        files.isNotEmpty()
    }

    @Throws(IOException::class, FTPConnectionClosedException::class)
    fun directoryExists(parentPath: String, dirName: String) = connectionExceptionWrapper {
        val directories = ftp.listDirectories(parentPath)
        if (directories.isEmpty()) {
            false
        } else {
            directories.find { it.name == dirName } != null
        }
    }

    @Throws(IOException::class, FTPConnectionClosedException::class)
    fun listFiles(path: String): List<String> = connectionExceptionWrapper { ftp.listFiles(path).map { it.name } }

    /**
     * Creates ONLY ONE new directory inside EXISTING one. Does not create many nested new directories at once.
     * @param path Either absolute or relative to the root dir path
     */
    open fun createDir(path: String): Boolean {
        return try {
            val parser = PathParser(path)
            val dirExists = parser.lastSubDir?.let { directoryExists(parser.lastSubDirParent, it) } ?: false
            if (dirExists) {
                true
            } else {
                ftp.makeDirectory(path)
            }
        } catch (e: IOException) {
            log.error("Cannot create path [$path]", e)
            false
        }
    }

    open fun createDirs(root: String, vararg dirs: String): Boolean {
        return try {
            tryCreateDirs(root, *dirs)
        } catch (e: IOException) {
            log.error("Cannot create path [${FileFactory.Separator("/").join(root, *dirs)}]", e)
            false
        }
    }

    open fun createEmptyFile(path: String): Boolean {
        return try {
            tryPrepareDirs(path)
            tryGetRemoteAsOutputStream(path) {}
            true
        } catch (e: IOException) {
            log.error("Cannot create empty remote file [$path]", e)
            false
        }
    }

    open fun uploadFile(file: File, path: String): Boolean {
        return try {
            tryPrepareDirs(path)
            tryUploadFile(file, path)
        } catch (e: IOException) {
            log.error("Cannot save file to $path", e)
            false
        }
    }

    open fun uploadFileFromInputStream(inputStream: InputStream, path: String): Boolean {
        return try {
            tryPrepareDirs(path)
            tryGetRemoteAsOutputStream(path) { inputStream.copyTo(it) }
            true
        } catch (e: IOException) {
            log.error("cannot create remote file [$path]", e)
            false
        }
    }

    open fun downloadFile(source: String, destination: String): Boolean {
        return try {
            tryDownloadFile(source, destination)
        } catch (e: IOException) {
            log.error("Cannot download file from $source", e)
            false
        }
    }

    open fun deleteFile(path: String): Boolean {
        return try {
            tryDeleteFile(path)
        } catch (e: IOException) {
            log.error("Cannot delete file [$path]", e)
            false
        }
    }

    @Throws(IOException::class)
    open fun tryUploadFile(file: File, path: String): Boolean {
        tryPrepareDirs(path)
        return connectionExceptionWrapper {
            FileInputStream(file).use {
                ftp.storeFile(path, it)
            }
        }
    }

    @Throws(IOException::class)
    open fun tryDownloadFile(source: String, destination: String): Boolean {
        return connectionExceptionWrapper {
            if (ftp.listFiles(source).isEmpty()) return@connectionExceptionWrapper false

            FileOutputStream(destination).use {
                ftp.retrieveFile(source, it)
            }
        }
    }

    /**
     * Make sure [processInputStream] lambda does not process incoming [InputStream] in a separate [Thread],
     * otherwise [InputStream] processing will be unexpectedly interrupted.
     *
     * There is no need to close incoming [InputStream]. It will be closed by this function
     * after [processInputStream] is completed.
     *
     * @throws IllegalReturnTypeException if return type == [InputStream]. It's forbidden to return
     * incoming [InputStream] since it will be closed by this function after [processInputStream]
     * block completed.
     */
    @Throws(IOException::class, FTPConnectionClosedException::class)
    open fun <T : Any> tryGetRemoteAsInputStream(source: String, processInputStream: (InputStream) -> T): T {
        return connectionExceptionWrapper {
            val iStream = ftp.retrieveFileStream(source) ?: throw IOException("cannot find remote file [$source]")
            val result: T = processInputStream(iStream)
            try {
                iStream.close()
            } catch (_: IOException) {
            }
            ftp.completePendingCommand()
            if (result is InputStream) throw IllegalReturnTypeException("InputStream is not allowed as a return type from this function")
            result
        }
    }

    /**
     * Make sure [processOutputStream] lambda does not process incoming [OutputStream] in a separate [Thread],
     * otherwise [OutputStream] processing will be unexpectedly interrupted.
     *
     * There is no need to close incoming [OutputStream]. It will be closed by this function
     * after [processOutputStream] is completed.
     */
    @Throws(IOException::class, FTPConnectionClosedException::class)
    open fun tryGetRemoteAsOutputStream(source: String, processOutputStream: (OutputStream) -> Unit) {
        tryPrepareDirs(source)
        connectionExceptionWrapper {
            val oStream = ftp.storeFileStream(source)
                    ?: throw IOException("cannot create OutputStream for remote resource [$source], ftpServer response: [${ftp.replyString}]")
            processOutputStream(oStream)
            try {
                oStream.close()
            } catch (_: IOException) {
            }
            ftp.completePendingCommand()
        }
    }

    @Throws(IOException::class, FTPConnectionClosedException::class)
    open fun tryDeleteFile(path: String) = ftp.deleteFile(path)

    protected open fun <T : Any> connectionExceptionWrapper(block: () -> T): T =
            try {
                block()
            } catch (e: FTPConnectionClosedException) {
                noConnectionException = false
                throw e
            }

    /**
     * Tries to create all nested directories if they don't exist
     */
    open fun tryPrepareDirs(path: String) {
        if (path.isBlank()) return
        val parser = PathParser(path, FileFactory.Separator("/"))
        val subDir = parser.lastSubDir ?: return
        if (directoryExists(parser.lastSubDirParent, subDir)) return
        tryCreateDirs(parser.root, *parser.subDirs.toTypedArray())
    }

    @Throws(IOException::class)
    open fun tryCreateDirs(root: String, vararg dirs: String): Boolean {
        var accumulator = root
        for (dir in dirs) {
            val path = if (accumulator.isEmpty()) dir else "$accumulator/$dir"
            if (!directoryExists(accumulator, dir) && !ftp.makeDirectory(path)) {
                log.error("Cannot create directory [$path], server reply = [${ftp.replyString}]")
                return false
            }
            accumulator = path
        }
        return true
    }

    companion object {
        private val log = LoggerFactory.getLogger(FtpClient::class.java)
    }
}