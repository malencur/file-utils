package com.malencur.file

import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

/**
 * Copies the content of project resource to the provided [OutputStream].
 *
 * Doesn't close [OutputStream] when copying is finished.
 *
 * @throws [IOException] if read/write failed
 */
@Throws(IOException::class)
fun Any.copyFromSource(source: String, output: OutputStream) {
    inputStreamFromResource(javaClass.classLoader, source).use {
        it.copyTo(output)
    }
}

/**
 * Creates [InputStream] from a project resource file or throws [IOException]
 * @return [InputStream] or throws [IOException]
 */
@Throws(IOException::class)
fun Any.inputStreamFromResource(fileName: String) = inputStreamFromResource(javaClass.classLoader, fileName)

/**
 * Creates [InputStream] from a resource that was loaded by supplied [ClassLoader]
 * @return [InputStream] or throws [IOException]
 */
@Throws(IOException::class)
fun inputStreamFromResource(classloader: ClassLoader, fileName: String): InputStream =
        run {
            classloader.getResource(fileName) ?: throw IOException("Cannot find resource [$fileName]")
        }.openStream()