package com.malencur.file

class IllegalReturnTypeException(msg: String) : RuntimeException(msg)