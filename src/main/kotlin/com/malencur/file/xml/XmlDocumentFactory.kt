package com.malencur.file.xml

import com.malencur.file.FileFactory
import com.malencur.file.FtpClient
import com.malencur.file.xml.property.XmlDocumentLazyProperty
import com.malencur.file.xml.property.XmlElementContext
import com.malencur.file.xml.property.XmlNodeElementHolder
import org.slf4j.LoggerFactory
import org.w3c.dom.DOMException
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.xml.sax.SAXException
import javax.xml.transform.TransformerException
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation

/**
 * Creates [org.w3c.dom.Document] instance from xml file and operates its content.
 *
 * @param onDocumentParseError A code bloc that will be run if xml document cannot be parsed.
 *
 * @since 1.0.0
 * @author alog
 */
open class XmlDocumentFactory(
        private val documentPropertyDelegate: XmlDocumentLazyProperty,
        fileName: String,
        rootDir: String,
        subDir: String? = null,
        init: Boolean = true,
        ftpClient: FtpClient? = null,
        private val onDocumentParseError: ((XmlDocumentLazyProperty) -> Unit)? = null
) : FileFactory(fileName, rootDir, subDir, ftpClient) {

    constructor(fileName: String, rootDir: String, subDir: String? = null, init: Boolean = true, ftpClient: FtpClient? = null, onDocumentParseError: ((XmlDocumentLazyProperty) -> Unit)? = null) :
            this(XmlDocumentLazyProperty(FileFactory.getDestinationFilePath(fileName, rootDir, subDir)), fileName, rootDir, subDir, init, ftpClient, onDocumentParseError)

    constructor(fileName: String, rootDir: String, subDir: String? = null, init: Boolean = true, onDocumentParseError: ((XmlDocumentLazyProperty) -> Unit)? = null) :
            this(fileName, rootDir, subDir, init, null, onDocumentParseError)

    val document by documentPropertyDelegate

    init {
        if (init) initDocument()
    }

    protected fun initDocument() {
        onDocumentParseError?.let { block ->
            try {
                document.normalizeDocument()
            } catch (ex: SAXException) {
                log.error("cannot parse xml", ex)
                block(documentPropertyDelegate)
            }
        }
    }

    fun setNewDocument(rootName: String) {
        documentPropertyDelegate.setNew(rootName)
    }

    fun saveDocument() {
        documentPropertyDelegate.persist()
    }

    open fun trySaveDocument(): Boolean {
        return try {
            documentPropertyDelegate.tryPersist()
            true
        } catch (ex: TransformerException) {
            false
        }
    }

    @Throws(ExceptionInInitializerError::class)
    protected fun getElement(nodeName: String) = getElement(document, nodeName)

    /**
     * Gets beck an instance of [clazz] whose xml element is the **only** child of the document root element.
     *
     * @throws IllegalArgumentException if document root element has more then one child node of the [clazz] type.
     * @throws ExceptionInInitializerError if document root element has no child node of the [clazz] type.
     */
    @Throws(IllegalArgumentException::class, ExceptionInInitializerError::class)
    protected fun <T : XmlNodeElementHolder> getRootElementSingletonChild(clazz: KClass<T>): T {
        return getElementSingletonChild(document, document.documentElement, clazz)
    }

    /**
     * Gets beck (if exists) or creates an instance of [clazz] whose xml element
     * is the **only** child of the document root element.
     *
     * **IMPORTANT:** The [clazz] element, if not already present inside document root,
     * will not be created and appended to the parent node until the very first call to its getter.
     *
     * @throws IllegalArgumentException if document root element has more then one child node of the [clazz] type.
     */
    @Throws(IllegalArgumentException::class)
    protected fun <T : XmlNodeElementHolder> createRootElementSingletonChild(clazz: KClass<T>): T {
        return getElementSingletonChild(document, document.documentElement, clazz) { _, _ -> null }
    }

    companion object {

        /**
         * Gets beck an instance of [clazz] whose xml element is the **only** child of the [element].
         *
         * @param document necessary for [clazz] creation
         * @param element A node whose child element will be searched and returned back inside [clazz] instance.
         * **Attention:** this element must be created by the supplied [document],
         * otherwise [DOMException]#WRONG_DOCUMENT_ERR exception will be thrown by created [clazz] later.
         * @param clazz An [XmlNodeElementHolder] subclass whose instance will be returned back.
         * @param fallBack **OPTIONAL:** Defines what to do if the [element] has no child node of the [clazz] type.
         * By default it throws [ExceptionInInitializerError]
         *
         * @throws IllegalArgumentException if [element] has more then one child node of the [clazz] type.
         * @throws ExceptionInInitializerError if [fallBack] is not provided and [element] has no child node of the [clazz] type.
         */
        @Throws(IllegalArgumentException::class, ExceptionInInitializerError::class)
        fun <T : XmlNodeElementHolder> getElementSingletonChild(
                document: Document,
                element: Element,
                clazz: KClass<T>,
                fallBack: ((document: Document, parentElement: Element) -> Element?) = { _, _ ->
                    val nodeName = clazz.findAnnotation<XmlNode>()?.name ?: clazz.simpleName!!.toLowerCase()
                    throw ExceptionInInitializerError("Cannot find node for nodeNames = [$nodeName]")
                }
        ): T {
            val nodeName = clazz.findAnnotation<XmlNode>()?.name ?: clazz.simpleName!!.toLowerCase()

            val elements = getNodeList(element, listOf(nodeName))?.toList() ?: emptyList()

            if (elements.size > 1) throw IllegalArgumentException("Xml root element has more than one node of type [$nodeName]")

            val instance = if (elements.size == 1) elements.first() else fallBack(document, element)

            return clazz(XmlElementContext(document, element, nodeName, instance))
        }

        @Throws(ExceptionInInitializerError::class)
        fun getElement(document: Document, nodeName: String) =
                getNode(document, nodeName)
                        ?: throw ExceptionInInitializerError("Cannot find node for nodeNames = [$nodeName]")

        private val log = LoggerFactory.getLogger(XmlDocumentFactory::class.java)
    }
}