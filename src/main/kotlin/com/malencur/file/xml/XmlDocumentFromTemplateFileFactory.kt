package com.malencur.file.xml

import com.malencur.file.xml.property.XmlDocumentLazyProperty
import java.io.IOException

/**
 * Creates [org.w3c.dom.Document] instance from xml file.
 *
 * If the file not found - generates default one from project resources.
 *
 * @param onDocumentParseError A code bloc that will be run if xml document cannot be parsed.
 *
 * @since 1.0.0
 * @author alog
 */
abstract class XmlDocumentFromTemplateFileFactory(
        fileName: String,
        rootDir: String,
        subDir: String? = null,
        onDocumentParseError: ((XmlDocumentLazyProperty) -> Unit)? = null
) : XmlDocumentFactory(fileName, rootDir, subDir, false, onDocumentParseError) {

    init {
        if (!fileExists) {
            if (!generateDefaultFileFromResources()) {
                throw IOException("Cannot create $fileName file.")
            }
        }
        initDocument()
    }
}