package com.malencur.file.xml.property

import org.w3c.dom.Element
import java.io.IOException
import kotlin.reflect.KProperty


/**
 * Delegate for xml node content string representation.
 *
 * @since 1.0.0
 * @author alog
 */
class XmlElementStringTextContent(element: Element, val default: String? = null):
        XmlSimplePropertyResolver(XmlPropertyType.TEXT_CONTENT, element) {

    /**
     * @throws IOException if the property not found and no default provided
     */
    @Throws(org.w3c.dom.DOMException::class, IOException::class)
    operator fun getValue(thisRef: Any?, property: KProperty<*>) = getTextContentValue(default) {it}

    @Throws(org.w3c.dom.DOMException::class)
    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
        element.textContent = value
    }
}