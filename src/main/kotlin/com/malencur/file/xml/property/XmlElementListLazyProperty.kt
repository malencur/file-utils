@file:Suppress("FunctionName")

package com.malencur.file.xml.property

import com.malencur.file.xml.invoke
import com.malencur.file.xml.map
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.NodeList
import kotlin.reflect.KClass
import kotlin.reflect.KProperty

/**
 * Provides lazy access to list of [XmlNodeElementHolder] subclass instances.
 *
 * @since 1.0.0
 * @author alog
 */
class XmlElementListLazyProperty<T : XmlNodeElementHolder>(
        private val document: Document,
        private val parentElement: Element,
        private val nodeList: NodeList,
        val clazz: KClass<T>,
        private val listener: XmlElementList.Listener? = null
) : XmlProperty(XmlPropertyType.NODE_LIST) {

    private val listObject: MutableList<T> by lazy {
        val immutableList = nodeList.map { element -> clazz(XmlElementContext(document, parentElement, element.nodeName, element)) }
        XmlElementList(immutableList, listener)
    }

    operator fun getValue(thisRef: Any?, property: KProperty<*>) = listObject
}
