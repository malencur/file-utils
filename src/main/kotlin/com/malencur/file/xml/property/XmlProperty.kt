package com.malencur.file.xml.property

/**
 * Defines xml property type.
 *
 * @since 1.0.0
 * @author alog
 */
abstract class XmlProperty(val xmPropertyType: XmlPropertyType)