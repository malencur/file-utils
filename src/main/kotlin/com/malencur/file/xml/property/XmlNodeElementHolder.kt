package com.malencur.file.xml.property

import com.malencur.file.xml.XmlDocumentFactory
import com.malencur.file.xml.XmlNode
import com.malencur.file.xml.getNodeList
import com.malencur.file.xml.invoke
import org.w3c.dom.Element
import java.io.IOException
import java.time.LocalDateTime
import kotlin.reflect.KClass
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.isSubclassOf
import kotlin.reflect.jvm.isAccessible

/**
 * Holds a particular node element.
 *
 * Provides convenient functions for property delegates instantiation.
 *
 * @since 1.0.0
 * @author alog
 */
abstract class XmlNodeElementHolder(context: XmlElementContext) {
    protected val document = context.document
    private val parentElement = context.parentElement

    /**
     * Represents this object in an xml file.
     *
     * **IMPORTANT:** This element, if not already present in xml, will not be created and appended to parent node
     * until the very first call to its getter. After that the [document] has to be saved to persist the changes.
     */
    val element by lazy {
        context.element
                ?: context.parentElement.appendChild(context.document.createElement(context.nodeName)) as Element
    }

    protected fun getChildrenNodeList(childNodeName: String) = getNodeList(element, listOf(childNodeName))
            ?: throw IOException("Parent node ${element.tagName}: cannot create [$childNodeName] children")

    /**
     * Creates [clazz] instance as well as its element in the [document].
     *
     * The [document] needs to be saved after this function call to persist the changes.
     *
     * @throws IllegalArgumentException if this object does not have list property of [clazz] type.
     */
    @Throws(IllegalArgumentException::class)
    fun <T : XmlNodeElementHolder> createNewListItemChild(clazz: KClass<T>): T {

        val list = getListProperty(clazz)

        val nodeName = clazz.findAnnotation<XmlNode>()?.name ?: clazz.simpleName!!.toLowerCase()
        val newChildElement = element.appendChild(document.createElement(nodeName)) as Element

        return clazz(XmlElementContext(document, element, nodeName, newChildElement))
                .also { list.add(it) }
    }

    /**
     * Checks if this object has a list property of [clazz] type and, if it does, resolves it
     * @return list property of [clazz] type
     * @throws IllegalArgumentException if this object does not have list property of [clazz] type.
     */
    @Throws(IllegalArgumentException::class)
    private fun <T : XmlNodeElementHolder> getListProperty(clazz: KClass<T>): XmlElementList<T> {
        val results = this.javaClass.kotlin.declaredMemberProperties.filter {
            it.isAccessible = true
            val delegate = it.getDelegate(this)
            delegate != null && delegate::class.isSubclassOf(XmlElementListLazyProperty::class)
        }.filter {
            val listDelegate = it.getDelegate(this) as XmlElementListLazyProperty<*>
            listDelegate.clazz == clazz
        }.map {
            it.get(this) as XmlElementList<T>
        }

        require(results.isNotEmpty()) { "${clazz.simpleName} is not a property of ${this::class.simpleName}" }

        return results.first()
    }

    /**
     * Gets beck (if exists) or creates an instance of [clazz] whose xml element is the **only** child of the current [element].
     *
     * **IMPORTANT:** The [clazz] element, if not already present inside this [element], will not be created
     * and appended to the parent node until the very first call to its getter.
     *
     * @throws IllegalArgumentException if current [element] has more then one child node of the [clazz] type.
     */
    @Throws(IllegalArgumentException::class)
    protected fun <T : XmlNodeElementHolder> getSingletonChild(clazz: KClass<T>): T {
        return XmlDocumentFactory.getElementSingletonChild(document, element, clazz) { _, _ -> null }
    }

    fun removeFromParent() {
        parentElement.removeChild(element)
    }

    protected fun stringAttribute(default: String? = null) = XmlElementStringAttribute(element, default)
    protected fun stringContent(default: String? = null) = XmlElementStringTextContent(element, default)

    protected fun boolAttribute(default: Boolean? = null) = XmlElementBooleanAttribute(element, default)
    protected fun boolContent(default: Boolean? = null) = XmlElementBooleanTextContent(element, default)

    protected fun intAttribute(default: Int? = null) = XmlElementIntegerAttribute(element, default)
    protected fun intContent(default: Int? = null) = XmlElementIntegerTextContent(element, default)

    protected fun longAttribute(default: Long? = null) = XmlElementLongAttribute(element, default)
    protected fun longContent(default: Long? = null) = XmlElementLongTextContent(element, default)

    protected fun dateAttribute(default: LocalDateTime? = null) = XmlElementLocalDateTimeAttribute(element, default)
    protected fun dateContent(default: LocalDateTime? = null) = XmlElementLocalDateTimeTextContent(element, default)

    protected fun <T : XmlNodeElementHolder> list(clazz: KClass<T>, listener: XmlElementList.Listener? = null): XmlElementListLazyProperty<T> {
        val nodeName = clazz.findAnnotation<XmlNode>()?.name ?: clazz.simpleName!!.toLowerCase()
        val nodeList = getChildrenNodeList(nodeName)
        return XmlElementListLazyProperty(document, element, nodeList, clazz, listener)
    }
}