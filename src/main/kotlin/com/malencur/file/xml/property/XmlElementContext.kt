package com.malencur.file.xml.property

import org.w3c.dom.Document
import org.w3c.dom.Element

/**
 * Contain necessary for node creation objects.
 *
 * @since 1.0.0
 * @author alog
 */
data class XmlElementContext(val document: Document, val parentElement: Element, val nodeName: String, val element: Element? = null)