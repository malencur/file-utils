package com.malencur.file.xml.property


/**
 * Extends ArrayList by adding change listeners.
 *
 * @since 1.0.0
 * @author alog
 */
class XmlElementList<T : XmlNodeElementHolder>(list: List<T> = emptyList(), listener: Listener? = null) : MutableList<T> {

    private val list = ArrayList<T>(list)

    override val size: Int
        get() = list.size

    private val handlers = if (listener == null) mutableListOf() else mutableListOf(listener)

    fun addListener(listener: Listener) {
        handlers.add(listener)
    }

    override fun contains(element: T) = list.contains(element)

    override fun containsAll(elements: Collection<T>) = list.containsAll(elements)

    override fun get(index: Int) = list[index]

    override fun indexOf(element: T) = list.indexOf(element)

    override fun isEmpty() = list.isEmpty()

    override fun iterator() = list.iterator()

    override fun lastIndexOf(element: T) = list.lastIndexOf(element)

    override fun listIterator() = list.listIterator()

    override fun listIterator(index: Int) = list.listIterator(index)

    override fun subList(fromIndex: Int, toIndex: Int) = list.subList(fromIndex, toIndex)

    override fun add(element: T): Boolean {
        val added = list.add(element)
        if (added) handlers.forEach { it.elementsAdded(1) }
        return added
    }

    override fun add(index: Int, element: T) {
        list.add(index, element)
        handlers.forEach { it.elementsAdded(1) }
    }

    override fun remove(element: T): Boolean {
        val removed = list.remove(element)
        if (removed) {
            element.removeFromParent()
            handlers.forEach { it.elementsRemoved(1) }
        }
        return removed
    }

    override fun removeAt(index: Int): T {
        val element = list.removeAt(index)
        element.removeFromParent()
        handlers.forEach { it.elementsRemoved(1) }
        return element
    }

    override fun addAll(elements: Collection<T>): Boolean {
        val added = list.addAll(elements)
        if (added) {
            var addedSize = 0
            elements.forEach {
                if (list.contains(it)) {
                    addedSize++
                }
            }
            handlers.forEach { it.elementsAdded(addedSize) }
        }
        return added
    }

    override fun addAll(index: Int, elements: Collection<T>): Boolean {
        val added = list.addAll(index, elements)
        if (added) {
            var addedSize = 0
            elements.forEach {
                if (list.contains(it)) {
                    addedSize++
                }
            }
            handlers.forEach { it.elementsAdded(addedSize) }
        }
        return added
    }

    override fun removeAll(elements: Collection<T>): Boolean {
        val removed = list.removeAll(elements)
        if (removed) {
            var removedSize = 0
            elements.forEach {
                if (!list.contains(it)) {
                    it.removeFromParent()
                    removedSize++
                }
            }
            handlers.forEach { it.elementsRemoved(removedSize) }
        }
        return removed
    }

    override fun clear() {
        val removedSize = size
        val temp = ArrayList(list)
        list.clear()
        temp.forEach { it.removeFromParent() }
        handlers.forEach { it.elementsRemoved(removedSize) }
    }

    override fun retainAll(elements: Collection<T>): Boolean {
        val previousSize = size
        val temp = ArrayList(list)
        val retained = list.retainAll(elements)
        if (retained) {
            temp.forEach { if (!list.contains(it)) it.removeFromParent() }
            val removedSize = previousSize - size
            handlers.forEach { it.elementsRemoved(removedSize) }
        }
        return retained
    }

    override fun set(index: Int, element: T): T {
        val replacedElement = list.set(index, element)
        replacedElement.removeFromParent()
        handlers.forEach { it.elementReplaced() }
        return replacedElement
    }

    interface Listener {
        fun elementsAdded(number: Int)
        fun elementsRemoved(number: Int)
        /**
         * Indicates that one element in the collection has been replaced by another
         */
        fun elementReplaced()
    }
}