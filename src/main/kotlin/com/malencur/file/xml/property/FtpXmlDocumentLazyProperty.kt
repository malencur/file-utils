package com.malencur.file.xml.property

import com.malencur.file.FtpClient
import org.apache.commons.net.ftp.FTPConnectionClosedException
import org.slf4j.LoggerFactory
import org.w3c.dom.Document
import org.xml.sax.SAXException
import java.io.IOException
import javax.xml.transform.OutputKeys
import javax.xml.transform.TransformerException
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult

class FtpXmlDocumentLazyProperty(filePath: String, private val ftpClient: FtpClient) : XmlDocumentLazyProperty(filePath) {


    override fun persist() {
        try {
            super.persist()
        } catch (e: IOException) {
            log.error("cannot get remote OutputStream for $filePath", e)
        }
    }

    /**
     * Persists document back into xml file or throws exception
     * @throws TransformerException if persisting is not successful
     * @throws FTPConnectionClosedException if ftp connection was closed
     * @throws IOException if writing to remote OutputStream was not successful
     */
    override fun tryPersist() {
        val doc = normalize() ?: return

        with(TransformerFactory.newInstance().newTransformer()) {
            setOutputProperty(OutputKeys.INDENT, "yes")
            val source = DOMSource(doc)
            ftpClient.tryGetRemoteAsOutputStream(filePath) {
                transform(source, StreamResult(it))
            }
        }
    }

    /**
     * Tries to parse xml file if present, or throws [IOException]
     * @throws SAXException if xml cannot be parsed
     * @throws FTPConnectionClosedException if ftp connection was closed
     * @throws IOException in any other case
     */
    override fun construct(): Document {
        return wrapInIOException {
            ftpClient.tryGetRemoteAsInputStream(filePath) {
                builder.parse(it)
            }
        }
    }

    companion object {
        private val log = LoggerFactory.getLogger(FtpXmlDocumentLazyProperty::class.java)
    }
}