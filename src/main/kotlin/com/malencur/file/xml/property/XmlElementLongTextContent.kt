package com.malencur.file.xml.property

import org.w3c.dom.Element
import java.io.IOException
import kotlin.reflect.KProperty

/**
 * Delegate for xml node content long representation.
 *
 * @since 1.0.0
 * @author alog
 */
class XmlElementLongTextContent(element: Element, val default: Long? = null) :
        XmlSimplePropertyResolver(XmlPropertyType.TEXT_CONTENT, element) {

    /**
     * @throws NumberFormatException if the attribute is not a valid representation of a number
     * @throws IOException if the property not found and no default provided
     */
    @Throws(NumberFormatException::class, org.w3c.dom.DOMException::class, IOException::class)
    operator fun getValue(thisRef: Any?, property: KProperty<*>) = getTextContentValue(default) { it.toLong() }

    @Throws(org.w3c.dom.DOMException::class)
    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Long) {
        element.textContent = value.toString()
    }
}