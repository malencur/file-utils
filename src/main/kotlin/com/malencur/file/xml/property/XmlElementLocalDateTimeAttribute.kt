package com.malencur.file.xml.property

import org.w3c.dom.Element
import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeParseException
import kotlin.reflect.KProperty

/**
 * Delegate for xml attribute [LocalDateTime] representation.
 *
 * @since 1.0.0
 * @author alog
 */
class XmlElementLocalDateTimeAttribute(element: Element, val default: LocalDateTime? = null):
        XmlSimplePropertyResolver(XmlPropertyType.ATTRIBUTE, element) {

    /**
     * @throws DateTimeParseException if the attribute is not a valid representation of a [LocalDateTime]
     * @throws IOException if the property not found and no default provided
     */
    @Throws(DateTimeParseException::class, IOException::class)
    operator fun getValue(thisRef: Any?, property: KProperty<*>): LocalDateTime =
            getAttributeValue(property, default) { LocalDateTime.parse(it) }

    @Throws(org.w3c.dom.DOMException::class)
    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: LocalDateTime) {
        element.setAttribute(getAttributeName(property), value.toString())
    }
}