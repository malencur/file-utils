package com.malencur.file.xml.property

import org.w3c.dom.Element
import java.io.IOException
import kotlin.reflect.KProperty


/**
 * Delegate for xml attribute string representation.
 *
 * @since 1.0.0
 * @author alog
 */
class XmlElementStringAttribute(element: Element, val default: String? = null):
        XmlSimplePropertyResolver(XmlPropertyType.ATTRIBUTE, element) {

    /**
     * @throws IOException if the property not found and no default provided
     */
    @Throws(IOException::class)
    operator fun getValue(thisRef: Any?, property: KProperty<*>) = getAttributeValue(property, default){it}

    @Throws(org.w3c.dom.DOMException::class)
    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
        element.setAttribute(getAttributeName(property), value)
    }
}