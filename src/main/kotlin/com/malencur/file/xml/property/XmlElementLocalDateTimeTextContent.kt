package com.malencur.file.xml.property

import org.w3c.dom.Element
import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeParseException
import kotlin.reflect.KProperty

/**
 * Delegate for xml node content [LocalDateTime] representation.
 *
 * @since 1.0.0
 * @author alog
 */
class XmlElementLocalDateTimeTextContent(element: Element, val default: LocalDateTime? = null):
        XmlSimplePropertyResolver(XmlPropertyType.TEXT_CONTENT, element) {

    /**
     * @throws DateTimeParseException if the attribute is not a valid representation of [LocalDateTime]
     * @throws IOException if the property not found and no default provided
     */
    @Throws(DateTimeParseException::class, org.w3c.dom.DOMException::class, IOException::class)
    operator fun getValue(thisRef: Any?, property: KProperty<*>): LocalDateTime =
            getTextContentValue(default) { LocalDateTime.parse(it.trim()) }

    @Throws(org.w3c.dom.DOMException::class)
    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: LocalDateTime) {
        element.textContent = value.toString()
    }
}