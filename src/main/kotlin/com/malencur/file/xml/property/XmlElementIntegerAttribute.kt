package com.malencur.file.xml.property

import org.w3c.dom.Element
import java.io.IOException
import kotlin.reflect.KProperty

/**
 * Delegate for xml attribute integer representation.
 *
 * @since 1.0.0
 * @author alog
 */
class XmlElementIntegerAttribute(element: Element, val default: Int? = null):
        XmlSimplePropertyResolver(XmlPropertyType.ATTRIBUTE, element) {
    /**
     * @throws NumberFormatException if the attribute is not a valid representation of a number
     * @throws IOException if the property not found and no default provided
     */
    @Throws(NumberFormatException::class, IOException::class)
    operator fun getValue(thisRef: Any?, property: KProperty<*>) = getAttributeValue(property, default){it.toInt()}

    @Throws(org.w3c.dom.DOMException::class)
    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Int) {
        element.setAttribute(getAttributeName(property), value.toString())
    }
}