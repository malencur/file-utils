package com.malencur.file.xml.property

import org.apache.commons.net.ftp.FTPConnectionClosedException
import org.slf4j.LoggerFactory
import org.w3c.dom.Document
import org.xml.sax.SAXException
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.OutputKeys
import javax.xml.transform.TransformerException
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult
import kotlin.reflect.KProperty

/**
 * Provides lazy access to [org.w3c.dom.Document] instance
 *
 * @since 1.0.0
 * @author alog
 */
open class XmlDocumentLazyProperty(protected val filePath: String) : XmlProperty(XmlPropertyType.DOCUMENT) {

    protected val builder: DocumentBuilder
        get() = DocumentBuilderFactory.newInstance().newDocumentBuilder()

    private var document: Document? = null

    operator fun getValue(thisRef: Any?, property: KProperty<*>): Document {
        return document ?: construct().also {
            document = it
        }
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Document) {
        document = value
    }

    /**
     * Persists document back into xml file
     */
    open fun persist() {
        try {
            tryPersist()
        } catch (ex: TransformerException) {
            log.error("cannot persist $filePath", ex)
        }
    }

    /**
     * Persists document back into xml file or throws exception
     * @throws TransformerException if persisting is not successful
     */
    open fun tryPersist() {
        val doc = normalize() ?: return

        with(TransformerFactory.newInstance().newTransformer()) {
            setOutputProperty(OutputKeys.INDENT, "yes")
            val source = DOMSource(doc)
            val result = StreamResult(File(filePath))
            transform(source, result)
        }
    }

    protected fun normalize(): Document? {
        return document?.apply {
            /*
            optional, but recommended.
            read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
             */
            normalize()
        }
    }

    /**
     * Tries to parse xml file if present, or throws [IOException]
     * @throws SAXException if xml cannot be parsed
     * @throws IOException in any other case
     */
    @Throws(SAXException::class, IOException::class)
    internal open fun construct(): Document {
        return wrapInIOException {
            builder.parse(File(filePath))
        }
    }

    /**
     *
     * @throws SAXException if xml cannot be parsed
     * @throws IOException or its subtype (like: [FileNotFoundException], [FTPConnectionClosedException]) in any other case
     */
    @Throws(SAXException::class, IOException::class)
    protected fun <T : Any> wrapInIOException(block: () -> T): T {
        try {
            return block()
        } catch (ex: IllegalArgumentException) {
            throw IOException(ex)
        } catch (ex: NullPointerException) {
            throw IOException(ex)
        }
    }

    fun setNew(rootName: String) {
        document = builder.newDocument().apply {
            appendChild(createElement(rootName))
        }
    }

    companion object {
        private val log = LoggerFactory.getLogger(XmlDocumentLazyProperty::class.java)
    }
}