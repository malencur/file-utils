package com.malencur.file.xml.property

import com.malencur.file.xml.XmlAttribute
import org.w3c.dom.Element
import java.io.IOException
import kotlin.reflect.KProperty
import kotlin.reflect.full.findAnnotation

/**
 * Defines xml property getters.
 *
 * @since 1.0.0
 * @author alog
 */
abstract class XmlSimplePropertyResolver(xmPropertyType: XmlPropertyType, protected val element: Element) : XmlProperty(xmPropertyType) {

    /**
     * @throws IOException if the property not found and no defaults provided
     */
    protected fun <T> getAttributeValue(property: KProperty<*>, default: T? = null, getFromString: (attribute: String) -> T): T {
        val attribute = element.getAttribute(getAttributeName(property))
        if (attribute.isBlank()) {
            if (default != null) return default
            throw IOException("Cannot find ${property.name} property")
        }
        return getFromString(attribute)
    }

    /**
     * @throws IOException if the property not found and no defaults provided
     */
    protected fun <T> getTextContentValue(default: T? = null, getFromString: (content: String) -> T): T {
        val content = element.textContent
        if (content.isNullOrBlank()) {
            if (default != null) return default
            throw IOException("No textContent for xml element: [$element]")
        }
        return getFromString(content.trim())
    }

    protected fun getAttributeName(property: KProperty<*>) = property.findAnnotation<XmlAttribute>()?.name
            ?: property.name
}