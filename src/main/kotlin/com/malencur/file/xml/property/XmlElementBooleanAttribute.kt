package com.malencur.file.xml.property

import org.w3c.dom.Element
import java.io.IOException
import kotlin.reflect.KProperty

/**
 * Delegate for xml attribute boolean representation.
 *
 * @since 1.0.0
 * @author alog
 */
class XmlElementBooleanAttribute(element: Element, val default: Boolean? = null):
        XmlSimplePropertyResolver(XmlPropertyType.ATTRIBUTE, element) {
    /**
     * Parses the string argument as a boolean.
     *
     * The boolean returned represents the value TRUE if the string argument is not null and is equal,
     * ignoring case, to the string "true". Otherwise the returned boolean will be always FALSE.
     *
     * @see [java.lang.Boolean.parseBoolean]
     *
     * @throws IOException if the property not found and no default provided
     */
    @Throws(IOException::class)
    operator fun getValue(thisRef: Any?, property: KProperty<*>) = getAttributeValue(property, default){it.toBoolean()}

    @Throws(org.w3c.dom.DOMException::class)
    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Boolean) {
        element.setAttribute(getAttributeName(property), value.toString())
    }
}