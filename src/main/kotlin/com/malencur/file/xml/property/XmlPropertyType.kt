package com.malencur.file.xml.property

enum class XmlPropertyType {
    DOCUMENT, NODE, NODE_LIST, ATTRIBUTE, TEXT_CONTENT
}