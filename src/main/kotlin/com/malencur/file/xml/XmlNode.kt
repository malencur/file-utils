package com.malencur.file.xml

/**
 * Specifies the xml node name that has to be associated with a kotlin bean.
 *
 * Use this annotation for beans whose xml node names are different from
 * their lowercase class names.
 */
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
annotation class XmlNode(val name: String)