package com.malencur.file.xml

/**
 * Specifies the xml node attribute name that has to be associated with a kotlin property.
 *
 * Use this annotation for properties whose xml attribute names are different from
 * their lowercase names.
 */
@Target(AnnotationTarget.PROPERTY)
@MustBeDocumented
annotation class XmlAttribute(val name: String)