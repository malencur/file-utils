package com.malencur.file.xml

import com.malencur.file.xml.property.XmlElementContext
import com.malencur.file.xml.property.XmlNodeElementHolder
import org.w3c.dom.Element
import org.w3c.dom.NodeList
import kotlin.reflect.KClass

inline fun <T : Any> NodeList.map(lambda: (element: Element) -> T?) = this.toList().mapNotNull {
    try {
        lambda(it)
    } catch (ignored: Exception) {
        null
    }
}

fun NodeList.toList(): List<Element> {

    val listObject = ArrayList<Element>()

    var i = 0

    while (i < this.length) {
        val element = this.item(i) as Element
        listObject.add(element)
        ++i
    }
    return listObject
}

operator fun <T : XmlNodeElementHolder> KClass<T>.invoke(context: XmlElementContext): T {
    val constructor = this.java.getConstructor(XmlElementContext::class.java)
    return constructor.newInstance(context)
}