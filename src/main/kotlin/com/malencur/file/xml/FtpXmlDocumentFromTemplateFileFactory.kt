package com.malencur.file.xml

import com.malencur.file.FtpClient
import com.malencur.file.xml.property.XmlDocumentLazyProperty
import java.io.IOException

/**
 * Creates [org.w3c.dom.Document] instance from xml file.
 *
 * If file not found - generates default one from project resources.
 *
 * @param onDocumentParseError A code bloc that will be run if xml document cannot be parsed.
 *
 * @author alog
 */
abstract class FtpXmlDocumentFromTemplateFileFactory(
        ftpClient: FtpClient,
        fileName: String,
        dirs: String,
        onDocumentParseError: ((XmlDocumentLazyProperty) -> Unit)? = null
) : FtpXmlDocumentFactory(ftpClient, fileName, dirs, false, onDocumentParseError) {

    init {
        if (!fileExists) {
            if (!generateDefaultFileFromResources()) {
                throw IOException("Cannot create $fileName file.")
            }
        }
        initDocument()
    }
}