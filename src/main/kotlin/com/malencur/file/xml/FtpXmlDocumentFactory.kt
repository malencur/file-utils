package com.malencur.file.xml

import com.malencur.file.FtpClient
import com.malencur.file.xml.property.FtpXmlDocumentLazyProperty
import com.malencur.file.xml.property.XmlDocumentLazyProperty
import org.apache.commons.net.ftp.FTPConnectionClosedException
import java.io.IOException

/**
 * Creates [org.w3c.dom.Document] instance from remote xml file and operates its content.
 * @throws FTPConnectionClosedException if ftp connection was closed
 * @throws IOException in any other case
 */
open class FtpXmlDocumentFactory(
        protected val ftpClient: FtpClient,
        fileName: String,
        dirs: String,
        init: Boolean = true,
        onDocumentParseError: ((XmlDocumentLazyProperty) -> Unit)? = null
) : XmlDocumentFactory(FtpXmlDocumentLazyProperty("$dirs/$fileName", ftpClient), fileName, dirs, null, init, ftpClient, onDocumentParseError) {

    override fun trySaveDocument(): Boolean {
        return try {
            super.trySaveDocument()
        } catch (ex: IOException) {
            if (!(ftpClient.connected && ftpClient.available)) {
                ftpClient.close()
                ftpClient.reconnect()
            }
            false
        }
    }
}