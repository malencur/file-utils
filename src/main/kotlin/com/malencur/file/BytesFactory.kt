package com.malencur.file

import java.io.IOException

/**
 * Creates [ByteArray] from a project resource file or throws [IOException]
 * @return [ByteArray] for requested resource or throws [IOException]
 */
@Throws(IOException::class)
fun Any.bytesFromResource(path: String): ByteArray = bytesFromResource(javaClass.classLoader, path)

/**
 * Creates [ByteArray] from a resource that was loaded by supplied [ClassLoader]
 * @return [ByteArray] for requested resource or throws [IOException]
 */
@Throws(IOException::class)
fun bytesFromResource(classloader: ClassLoader, path: String): ByteArray =
        inputStreamFromResource(classloader, path).readBytes()