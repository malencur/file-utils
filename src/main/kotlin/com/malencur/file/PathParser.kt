package com.malencur.file

import java.io.IOException

class PathParser(path: String, private val separator: FileFactory.Separator = FileFactory.Separator("/")) {

    val root: String
        get() = strategy.root

    val subDirs: List<String>
        get() = strategy.subDirs

    val file: String
        get() = strategy.file

    val directoryPath: String
        get() = if (root.isEmpty()) {
            if (subDirs.isEmpty())
                ""
            else
                subDirs.reduce { acc, s -> acc + separator.value + s }
        } else {
            subDirs.fold(root) { acc, s -> acc + separator.value + s }
        }

    val lastSubDirParent: String
        get() {
            return when (subDirs.size) {
                0, 1 -> root
                else -> if (root.isEmpty()) {
                    subDirs.dropLast(1).reduce { acc, s -> acc + separator.value + s }
                } else {
                    subDirs.dropLast(1).fold(root) { acc, s -> acc + separator.value + s }
                }
            }
        }

    val lastSubDir: String?
        get() {
            return when (subDirs.size) {
                0 -> null
                else -> subDirs.last()
            }
        }

    private val strategy: PathParserStrategy

    init {
        if (path.isBlank())
            throw IOException("path is empty")

        if (separator.value.isBlank())
            throw IOException("separator's value is empty")

        val trimmed = path.trim()

        val items = trimmed.split(separator.value)
        val size = items.size
        strategy = when (size) {
            1 -> OneItemsStrategy(items)
            2 -> TwoItemsStrategy(trimmed, items)
            3 -> ThreeItemsStrategy(items)
            else -> ManyItemsStrategy(items)
        }
    }
}

internal abstract class PathParserStrategy {
    abstract val root: String
    abstract val subDirs: List<String>
    abstract val file: String

    val String.isFile: Boolean
        get() = matches("^[^.|\\s]+\\.[^.]*".toRegex()) //pattern: [something.something]
}

internal class OneItemsStrategy(items: List<String>) : PathParserStrategy() {
    override val root: String
    override val subDirs: List<String>
    override val file: String

    init {
        assert(items.size == 1)
        root = ""
        if (items.first().isFile) {
            subDirs = emptyList()
            file = items.first()
        } else {
            subDirs = listOf(items.first())
            file = ""
        }
    }

}

internal class TwoItemsStrategy(path: String, items: List<String>) : PathParserStrategy() {
    override val root: String
    override val subDirs: List<String>
    override val file: String

    init {
        assert(items.size == 2)
        if (items.last().isFile) {
            if (items.first().isBlank()) {
                root = ""
                subDirs = emptyList()
                file = items.last()
            } else {
                root = ""
                subDirs = listOf(items.first())
                file = items.last()
            }
        } else {
            file = ""
            if (items.first().isBlank()) {
                root = "/${items.last().trim()}"
                subDirs = emptyList()
            } else {
                root = ""
                subDirs = items
            }
        }
    }

}

internal class ThreeItemsStrategy(items: List<String>) : PathParserStrategy() {
    override val root: String
    override val subDirs: List<String>
    override val file: String

    init {
        assert(items.size == 3)
        if (items.last().isFile) {
            file = items.last()
            if (items.first().isBlank()) {
                root = "/${items[1]}"
                subDirs = emptyList()
            } else {
                root = ""
                subDirs = items.dropLast(1)
            }
        } else {
            file = ""
            if (items.first().isBlank()) {
                root = "/${items[1]}"
                subDirs = listOf(items.last())
            } else {
                root = ""
                subDirs = items
            }
        }
    }
}

internal class ManyItemsStrategy(items: List<String>) : PathParserStrategy() {
    override val root: String
    override val subDirs: List<String>
    override val file: String

    init {
        assert(items.size > 3)
        if (items.last().isFile) {
            file = items.last()
            if (items.first().isBlank()) {
                root = "/${items[1]}"
                subDirs = items.filterIndexed { index, _ -> index in 2 until items.size - 1 }
            } else {
                root = ""
                subDirs = items.dropLast(1)
            }

        } else {
            file = ""
            if (items.first().isBlank()) {
                root = "/${items[1]}"
                subDirs = items.filterIndexed { index, _ -> index in 2 until items.size }
            } else {
                root = ""
                subDirs = items
            }
        }
    }
}
