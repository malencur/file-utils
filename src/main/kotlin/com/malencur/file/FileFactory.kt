package com.malencur.file

import com.malencur.file.FileFactory.Companion.fileFromInputStream
import com.malencur.file.FileFactory.Companion.fileFromResource
import org.slf4j.LoggerFactory
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream

@Suppress("MemberVisibilityCanBePrivate", "unused")

/**
 *
 * Provides file creation functionality
 *
 * @property fileName New file name
 * @property rootDir Complete path to the root directory
 * @property subDir Optional. Subdirectory to the [rootDir]
 *
 * @since 1.0.0
 * @author alog
 */
open class FileFactory(protected val fileName: String, private val rootDir: String, private val subDir: String? = null, private val ftpClient: FtpClient? = null) {
    init {
        if (fileName.isEmpty()) throw IOException("You must pass in fileName")
        if (ftpClient != null) {
            if (!(ftpClient.connected && ftpClient.available)) {
                throw IOException("Unacceptable ftpClient state connected = [${ftpClient.connected}], available = [${ftpClient.available}]")
            }
        } else {
            if (rootDir.isEmpty()) throw IOException("You must pass in rootDir")
        }
    }

    /**
     * Complete destination path
     */
    protected val destinationFilePath: String
        get() = getDestinationFilePath(fileName, rootDir, subDir, run { if (ftpClient != null) Separator("/") else null })

    protected val fileExists: Boolean
        get() = fileExists(destinationFilePath, ftpClient)

    /**
     * @return TRUE - if the file has been deleted
     */
    protected fun deleteFile() = deleteFile(destinationFilePath, ftpClient)

    protected fun createEmptyFile() = ftpClient?.createEmptyFile(destinationFilePath)
            ?: createEmptyFile(destinationFilePath)

    /**
     * Copies file from internal project resources folder to [destinationFilePath]
     */
    protected fun generateDefaultFileFromResources() = generateDefaultFile(fileName, destinationFilePath, this.javaClass.classLoader, ftpClient)

    companion object {

        internal val log = LoggerFactory.getLogger(FileFactory::class.java)

        /**
         * @return TRUE - if file does exist
         * @throws IOException if [FtpClient] throws any exception
         */
        @Throws(IOException::class)
        fun fileExists(filePath: String, ftpClient: FtpClient? = null) = ftpClient?.fileExists(filePath)
                ?: File(filePath).exists()

        /**
         * @return TRUE - if the file has been deleted, FALSE - if operation was not successful (E.g.: due to [FtpClient] connection failure)
         */
        fun deleteFile(filePath: String, ftpClient: FtpClient? = null) = ftpClient?.deleteFile(filePath)
                ?: File(filePath).delete()

        /**
         * Creates an empty file for the [filePath] including any
         * necessary but nonexistent parent directories.
         *
         * @param filePath Complete path to the file including its name with extension
         */
        fun createEmptyFile(filePath: String):Boolean{
            val file = File(filePath)
            var dirCreated = true
            var newFileCreated = false

            try {
                if (!file.parentFile.exists())
                    dirCreated = file.parentFile.mkdirs()
                newFileCreated = file.createNewFile()
            } catch (ex: Exception) {
                log.error("Cannot create empty file [$filePath]", ex)
            }

            return dirCreated && newFileCreated
        }

        /**
         * Copies file from internal project resources folder to [destinationFilePath].
         * Creates any necessary but nonexistent destination parent directories.
         *
         * @param resourceFileName The file name that is located inside project resources
         * @param destinationFilePath Complete path to a new file destination (includes file name with extension)
         * @param classLoader Classloader that has access to the project resources
         * @param ftpClient OPTIONAL If present the file will be generated on a remote server using this client
         * @return TRUE - if a new file has been generated
         */
        fun generateDefaultFile(resourceFileName: String, destinationFilePath: String, classLoader: ClassLoader, ftpClient: FtpClient? = null): Boolean {
            if (ftpClient != null && !(ftpClient.connected && ftpClient.available)) {
                log.error("Cannot generate default file. Unacceptable FtpClient state connected = [${ftpClient.connected}], available = [${ftpClient.available}]")
                return false
            }
            try {
                if (fileExists(destinationFilePath, ftpClient)) return false
            } catch (e: IOException) {
                log.error("Cannot generate default remote file [$destinationFilePath]", e)
                return false
            }

            return classLoader.getResourceAsStream(resourceFileName)?.use { inputStream ->
                ftpClient?.uploadFileFromInputStream(inputStream, destinationFilePath)
                        ?: run {
                            createEmptyFile(destinationFilePath)
                            inputStream.copyToFile(destinationFilePath)
                        }
            } ?: false
        }

        /**
         * Defines the complete path to the specified [dirName]
         * as if it was located inside users home folder.
         *
         * @return [users home folder]/[specified dirName] path
         */
        fun getDirPathInUsersHome(dirName: String) = System.getProperty("user.home") + File.separator + dirName

        /**
         * Creates directory inside users home folder named by [dirName], including any
         * necessary but nonexistent parent directories.  Note that if this
         * operation fails it may have succeeded in creating some of the necessary
         * parent directories.
         *
         * @param dirName Directory name, including necessary parent directories
         *
         * @return Created dir complete path OR empty string if creation failed
         */
        fun createDirInUsersHome(dirName: String): String {
            var created = false
            val dirPath = getDirPathInUsersHome(dirName)

            if (fileExists(dirPath)) return dirPath

            try {
                created = File(dirPath).mkdirs()
            } catch (ex: Exception) {
                log.error(ex.message, ex.cause)
            }
            if (created) return dirPath
            return ""
        }

        /**
         * @return Complete destination path
         */
        fun getDestinationFilePath(fileName: String, rootDir: String, subDir: String? = null, separator: Separator? = null): String {
            return run { separator ?: Separator(File.separator) }.let {
                if (null != subDir) {
                    it.join(rootDir, subDir, fileName)
                } else {
                    it.join(rootDir, fileName)
                }
            }
        }

        fun joinWithFileSeparator(vararg names: String): String = names.joinToString(File.separator)

        /**
         * Creates [File] from a project resource file or throws [IOException]
         * @return [File] from requested resource or throws [IOException]
         */
        @Throws(IOException::class)
        fun fileFromResource(classloader: ClassLoader, path: String): File = File(uriForResource(classloader, path))

        /**
         * Creates [File] from the given [InputStream] and writes it to [destination].
         *
         * **Does not closes the incoming [InputStream]**. It is the caller responsibility to do that.
         *
         * @return [File] from the given [InputStream]
         * @throws [IOException] if read/write operation failed
         */
        @Throws(IOException::class)
        fun fileFromInputStream(input: InputStream, destination: String): File =
                File(destination).apply {
                    outputStream().use { output ->
                        input.copyTo(output)
                    }
                }
    }

    data class Separator(val value: String)
}

fun FileFactory.Separator.join(vararg names: String) = names.joinToString(value)

/**
 * Creates [File] from a project resource file or throws [IOException]
 * @return [File] from requested resource or throws [IOException]
 */
@Throws(IOException::class)
fun Any.fileFromResource(path: String): File = fileFromResource(javaClass.classLoader, path)

/**
 * Creates [File] from the given [InputStream] and writes it to [destination]
 * @return [File] from the given [InputStream]
 * @throws [IOException] if read/write operation failed
 */
@Throws(IOException::class)
fun InputStream.toFile(destination: String): File = fileFromInputStream(this, destination)

/**
 * Does not close the [InputStream]
 */
fun InputStream.copyToFile(path: String): Boolean =
        try {
            FileOutputStream(path).use {
                copyTo(it)
            }
            true
        } catch (e: Exception) {
            FileFactory.log.error("Cannot create file [$path]", e)
            false
        }