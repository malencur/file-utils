package com.malencur.file

import org.slf4j.LoggerFactory
import java.io.BufferedInputStream
import java.io.InputStream
import java.util.concurrent.Executors
import javax.sound.sampled.*

@Suppress("unused")

/**
 * Provides audio stream operation.
 *
 * @since 1.0.0
 * @author alog
 */
class Audio(private val audioFileName: String, private val streamBuilder: (fileName: String) -> InputStream): AutoCloseable{

    internal var audioFormat: AudioFormat? = null
    internal var audioInputStream: AudioInputStream? = null
    internal var sourceDataLine: SourceDataLine? = null
    internal var stopPlayback = false
    internal var playbackFinished = true
    private var dataLineInfo: DataLine.Info? = null
    private val executorService = Executors.newSingleThreadExecutor()

    fun play(): SourceDataLine? {
        if (playbackFinished) {
            audioInputStream = AudioSystem.getAudioInputStream(BufferedInputStream(streamBuilder(audioFileName)))
            audioFormat = audioInputStream?.format
            dataLineInfo = DataLine.Info(SourceDataLine::class.java, audioFormat)
            sourceDataLine = AudioSystem.getLine(dataLineInfo) as SourceDataLine

            executorService.submit(PlayBack())

            return sourceDataLine
        }
        return null
    }

    /**
     * Stops playback.
     *
     * Because of the variables buffers involved inside [PlayBack] thread,
     * there will normally be a delay between this function call and the actual termination of playback.
     */
    fun stop() {
        stopPlayback = true
    }

    /**
     * Create a thread to Play back the variables and start it running.
     * It will run until the end of file, or the Stop button is clicked, whichever occurs first.
     */
    internal inner class PlayBack : Runnable{
        private var tempBuffer = ByteArray(10000)
        private var readFromInputStream: Int? = 0

        override fun run() {
            playbackFinished = false
            try {
                sourceDataLine?.open(audioFormat)
                sourceDataLine?.start()
                /*
                Keep looping until the input read method returns -1 for empty
                stream or the user clicks the Stop button causing stopPlayback
                to switch from false to true.
                 */
                while (!stopPlayback) {
                    readFromInputStream = audioInputStream?.read(tempBuffer, 0, tempBuffer.size)
                    if (readFromInputStream == -1)
                        break
                    if (readFromInputStream!! > 0) {
                        /*
                        Write variables to the internal buffer of the
                        variables line where it will be delivered to the speaker.
                         */
                        sourceDataLine?.write(tempBuffer, 0, readFromInputStream as Int)
                    }
                }
                audioInputStream?.close()
                /*
                Block and wait for internal buffer
                 of the variables line to empty.
                 */
                sourceDataLine?.drain()
                sourceDataLine?.close()

            } catch (e: Exception) {
                log.error(e.message, e.cause)
            }


            playbackFinished = true

            //Prepare to playback another file
            // stopBtn.setEnabled(false);
            //playBtn.setEnabled(true);
            stopPlayback = false
        }
    }

    /**
     * Frees audio [PlayBack] thread.
     *
     * Without this method call an object of this class may prevent a process from termination
     * (happens at least when tested on a local machine with Spek plugin)
     */
    override fun close() {
        executorService.shutdown()
    }

    companion object {
        private val log = LoggerFactory.getLogger(Audio::class.java)
    }
}