package com.malencur.file

import java.io.IOException

@Suppress("unused")

/**
 *
 * Ensures that there is a file that corresponds to provided [fileName]
 * If the file not present - generates default one from project resources.
 *
 * @throws IOException If the file cannot be created.
 *
 * @since 1.0.0
 * @author alog
 */
abstract class TemplateFileFactory(fileName: String, rootDir: String, subDir: String? = null) :
        FileFactory(fileName, rootDir, subDir) {

    init {
        if (!fileExists) {
            if (!generateDefaultFileFromResources()) {
                throw IOException("Cannot create $fileName file.")
            }
        }
    }
}