package com.malencur.file.properties

import com.malencur.file.*
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import java.io.File
import java.io.IOException
import java.time.LocalDateTime
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

object ExternalPropertiesFromDocumentTest : Spek({
    FileFactory.deleteFile(externalDir + File.separator + PROPERTIES_FILE_NAME)
    val props = ExternalPropertiesHolder()
    describe("string property") {
        context("existing key") {
            on("resolving") {
                it("should return the value") {
                    assertEquals(props.str, "дії")
                }
            }
            on("setting new value") {
                val newValue = "93-ї бригади"
                props.str = newValue
                val newProps = ExternalPropertiesHolder()
                it("should save it") {
                    assertEquals(newProps.str, newValue)
                }
                it("should return new value") {
                    assertEquals(props.str, newValue)
                }
            }
        }
        context("none existing key") {
            on("resolving with default") {
                it("should return the default") {
                    assertEquals(props.noStrDefault, DEFAULT_STRING_VALUE)
                }
            }
            on("resolving without default") {
                it("should return the default") {
                    assertFailsWith(IOException::class) {
                        props.noStr
                    }
                }
            }
        }
    }
    describe("string list property") {
        context("existing key") {
            on("resolving") {
                val expected = listOf("one", "two", "three", "four")
                it("should return the value") {
                    assertEquals(props.strList, expected)
                }
            }
            on("setting new value") {
                val newValue = listOf("one", "two", "three", "four", "five")
                props.strList = newValue
                val newProps = ExternalPropertiesHolder()
                it("should save it") {
                    assertEquals(newProps.strList, newValue)
                }
                it("should return new value") {
                    assertEquals(props.strList, newValue)
                }
            }
        }
        context("none existing key") {
            on("resolving with default") {
                it("should return the default") {
                    assertEquals(props.noStrListDefault, DEFAULT_STRING_LIST_VALUE)
                }
            }
            on("resolving without default") {
                it("should return the default") {
                    assertFailsWith(IOException::class) {
                        props.noStrList
                    }
                }
            }
        }
    }
    describe("integer property") {
        context("existing key") {
            on("resolving") {
                it("should return the value") {
                    assertEquals(props.int, 10)
                }
            }
            on("setting new value") {
                val newValue = 20
                props.int = newValue
                val newProps = ExternalPropertiesHolder()
                it("should save it") {
                    assertEquals(newProps.int, newValue)
                }
                it("should return new value") {
                    assertEquals(props.int, newValue)
                }
            }
        }
        context("none existing key") {
            on("resolving with default") {
                it("should return the default") {
                    assertEquals(props.noIntDefault, DEFAULT_INTEGER_VALUE)
                }
            }
            on("resolving without default") {
                it("should return the default") {
                    assertFailsWith(IOException::class) {
                        props.noInt
                    }
                }
            }
        }
    }
    describe("integer list property") {
        context("existing key") {
            on("resolving") {
                val expected = listOf(1, 2, 3, 4)
                it("should return the value") {
                    assertEquals(props.intList, expected)
                }
            }
            on("setting new value") {
                val newValue = listOf(1, 2, 3, 4, 5)
                props.intList = newValue
                val newProps = ExternalPropertiesHolder()
                it("should save it") {
                    assertEquals(newProps.intList, newValue)
                }
                it("should return new value") {
                    assertEquals(props.intList, newValue)
                }
            }
        }
        context("none existing key") {
            on("resolving with default") {
                it("should return the default") {
                    assertEquals(props.noIntListDefault, DEFAULT_INTEGER_LIST_VALUE)
                }
            }
            on("resolving without default") {
                it("should return the default") {
                    assertFailsWith(IOException::class) {
                        props.noIntList
                    }
                }
            }
        }
    }
    describe("boolean property") {
        context("existing key") {
            on("resolving good formatted value") {
                it("should return the actual value") {
                    assertEquals(props.bool, true)
                }
            }
            on("resolving bad formatted value") {
                it("should return false") {
                    assertEquals(props.boolBadFormatted, false)
                }
            }
            on("setting new value") {
                val newValue = false
                props.bool = newValue
                val newProps = ExternalPropertiesHolder()
                it("should save it") {
                    assertEquals(newProps.bool, newValue)
                }
                it("should return new value") {
                    assertEquals(props.bool, newValue)
                }
            }
        }
        context("none existing key") {
            on("resolving with default") {
                it("should return the default") {
                    assertEquals(props.noBoolDefault, DEFAULT_BOOLEAN_VALUE)
                }
            }
            on("resolving without default") {
                it("should return the default") {
                    assertFailsWith(IOException::class) {
                        props.noBool
                    }
                }
            }
        }
    }
    describe("localeDateTime property") {
        context("existing key") {
            on("resolving") {
                val expected = LocalDateTime.parse("2000-01-01T00:00:00")
                it("should return the value") {
                    assertEquals(props.date, expected)
                }
            }
            on("setting new value") {
                val newValue = LocalDateTime.parse("2000-01-01T12:05:00")
                props.date = newValue
                val newProps = ExternalPropertiesHolder()
                it("should save it") {
                    assertEquals(newProps.date, newValue)
                }
                it("should return new value") {
                    assertEquals(props.date, newValue)
                }
            }
        }
        context("none existing key") {
            on("resolving with default") {
                it("should return the default") {
                    assertEquals(props.noDateDefault, DEFAULT_TIME_VALUE)
                }
            }
            on("resolving without default") {
                it("should return the default") {
                    assertFailsWith(IOException::class) {
                        props.noDate
                    }
                }
            }
        }
    }
    describe("localeDateTime list property") {
        context("existing key") {
            on("resolving") {
                val expected = listOf(
                        LocalDateTime.parse("2000-01-01T12:00:00"),
                        LocalDateTime.parse("2000-01-01T13:00:00"),
                        LocalDateTime.parse("2000-01-01T14:00:00"),
                        LocalDateTime.parse("2000-01-01T15:00:00")
                )
                it("should return the value") {
                    assertEquals(props.dateList, expected)
                }
            }
            on("setting new value") {
                val newValue = listOf(
                        LocalDateTime.parse("2000-01-01T12:00:00"),
                        LocalDateTime.parse("2000-01-01T13:00:00"),
                        LocalDateTime.parse("2000-01-01T14:00:00"),
                        LocalDateTime.parse("2000-01-01T15:00:00"),
                        LocalDateTime.parse("2000-01-01T16:00:00")
                )
                props.dateList = newValue
                val newProps = ExternalPropertiesHolder()
                it("should save it") {
                    assertEquals(newProps.dateList, newValue)
                }
                it("should return new value") {
                    assertEquals(props.dateList, newValue)
                }
            }
        }
        context("none existing key") {
            on("resolving with default") {
                it("should return the default") {
                    assertEquals(props.noDateListDefault, DEFAULT_TIME_LIST_VALUE)
                }
            }
            on("resolving without default") {
                it("should return the default") {
                    assertFailsWith(IOException::class) {
                        props.noDateList
                    }
                }
            }
        }
    }
})