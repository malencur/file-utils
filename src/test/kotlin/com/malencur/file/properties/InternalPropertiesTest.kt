package com.malencur.file.properties

import com.malencur.file.DEFAULT_BOOLEAN_VALUE
import com.malencur.file.DEFAULT_INTEGER_VALUE
import com.malencur.file.DEFAULT_STRING_VALUE
import com.malencur.file.DEFAULT_TIME_VALUE
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import java.io.IOException
import java.time.LocalDateTime
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

object InternalPropertiesTest : Spek({
    val props = InternalPropertiesHolder()
    describe("string property") {
        context("existing key") {
            on("resolving") {
                it("should return the value") {
                    assertEquals(props.str, "дії")
                }
            }
        }
        context("none existing key") {
            on("resolving with default") {
                it("should return the default") {
                    assertEquals(props.noStrDefault, DEFAULT_STRING_VALUE)
                }
            }
            on("resolving without default") {
                it("should return the default") {
                    assertFailsWith(IOException::class) {
                        props.noStr
                    }
                }
            }
        }
    }
    describe("string list property") {
        context("existing key") {
            on("resolving") {
                val expected = listOf("one", "two", "three", "four")
                it("should return the value") {
                    assertEquals(props.strList, expected)
                }
            }
        }
        context("none existing key") {
            on("resolving with default") {
                it("should return the default") {
                    assertEquals(props.noStrListDefault, DEFAULT_STRING_LIST_VALUE)
                }
            }
            on("resolving without default") {
                it("should return the default") {
                    assertFailsWith(IOException::class) {
                        props.noStrList
                    }
                }
            }
        }
    }
    describe("integer property") {
        context("existing key") {
            on("resolving") {
                it("should return the value") {
                    assertEquals(props.int, 10)
                }
            }
        }
        context("none existing key") {
            on("resolving with default") {
                it("should return the default") {
                    assertEquals(props.noIntDefault, DEFAULT_INTEGER_VALUE)
                }
            }
            on("resolving without default") {
                it("should return the default") {
                    assertFailsWith(IOException::class) {
                        props.noInt
                    }
                }
            }
        }
    }
    describe("integer list property") {
        context("existing key") {
            on("resolving") {
                val expected = listOf(1, 2, 3, 4)
                it("should return the value") {
                    assertEquals(props.intList, expected)
                }
            }
        }
        context("none existing key") {
            on("resolving with default") {
                it("should return the default") {
                    assertEquals(props.noIntListDefault, DEFAULT_INTEGER_LIST_VALUE)
                }
            }
            on("resolving without default") {
                it("should return the default") {
                    assertFailsWith(IOException::class) {
                        props.noIntList
                    }
                }
            }
        }
    }
    describe("boolean property") {
        context("existing key") {
            on("resolving good formatted value") {
                it("should return the actual value") {
                    assertEquals(props.bool, true)
                }
            }
            on("resolving bad formatted value") {
                it("should return false") {
                    assertEquals(props.boolBadFormatted, false)
                }
            }
        }
        context("none existing key") {
            on("resolving with default") {
                it("should return the default") {
                    assertEquals(props.noBoolDefault, DEFAULT_BOOLEAN_VALUE)
                }
            }
            on("resolving without default") {
                it("should return the default") {
                    assertFailsWith(IOException::class) {
                        props.noBool
                    }
                }
            }
        }
    }
    describe("localeDateTime property") {
        context("existing key") {
            on("resolving") {
                val expected = LocalDateTime.parse("2000-01-01T00:00:00")
                it("should return the value") {
                    assertEquals(props.date, expected)
                }
            }
        }
        context("none existing key") {
            on("resolving with default") {
                it("should return the default") {
                    assertEquals(props.noDateDefault, DEFAULT_TIME_VALUE)
                }
            }
            on("resolving without default") {
                it("should return the default") {
                    assertFailsWith(IOException::class) {
                        props.noDate
                    }
                }
            }
        }
    }
    describe("localeDateTime list property") {
        context("existing key") {
            on("resolving") {
                val expected = listOf(
                        LocalDateTime.parse("2000-01-01T12:00:00"),
                        LocalDateTime.parse("2000-01-01T13:00:00"),
                        LocalDateTime.parse("2000-01-01T14:00:00"),
                        LocalDateTime.parse("2000-01-01T15:00:00")
                )
                it("should return the value") {
                    assertEquals(props.dateList, expected)
                }
            }
        }
        context("none existing key") {
            on("resolving with default") {
                it("should return the default") {
                    assertEquals(props.noDateListDefault, DEFAULT_TIME_LIST_VALUE)
                }
            }
            on("resolving without default") {
                it("should return the default") {
                    assertFailsWith(IOException::class) {
                        props.noDateList
                    }
                }
            }
        }
    }
})