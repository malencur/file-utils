package com.malencur.file.properties

import com.malencur.file.*
import com.malencur.file.properties.factory.ExternalPropertiesFromTemplateFileFactory
import com.malencur.file.properties.factory.InternalPropertiesFactory
import java.time.LocalDateTime

internal const val PROPERTIES_FILE_NAME = "properties.txt"
private const val SELECTOR = "com.malencur"
private const val LIST_SELECTOR = "com.malencur.list"
internal val DEFAULT_STRING_LIST_VALUE = listOf("defaultOne", "defaultTwo", "defaultThree")
internal val DEFAULT_INTEGER_LIST_VALUE = listOf(1, 2, 3)
internal val DEFAULT_TIME_LIST_VALUE = listOf(
        LocalDateTime.parse("2000-01-01T12:00:00"),
        LocalDateTime.parse("2000-01-01T13:00:00"),
        LocalDateTime.parse("2000-01-01T14:00:00")
)

class ExternalPropertiesHolder : ExternalPropertiesFromTemplateFileFactory(PROPERTIES_FILE_NAME, externalDir) {

    // single properties:
    @PropertyName("string")
    var str by stringPersistedProperty(SELECTOR, persisted = true)
    var noStr by stringPersistedProperty(SELECTOR)
    val noStrDefault by stringProperty(SELECTOR, DEFAULT_STRING_VALUE)

    @PropertyName("integer")
    var int by intPersistedProperty(SELECTOR, persisted = true)
    var noInt by intPersistedProperty(SELECTOR)
    val noIntDefault by intProperty(SELECTOR, DEFAULT_INTEGER_VALUE)

    @PropertyName("boolean")
    var bool by booleanPersistedProperty(SELECTOR, persisted = true)
    var noBool by booleanPersistedProperty(SELECTOR)
    val noBoolDefault by booleanProperty(SELECTOR, DEFAULT_BOOLEAN_VALUE)
    val boolBadFormatted by booleanProperty(SELECTOR)

    @PropertyName("localDateTime")
    var date by localDateTimePersistedProperty(SELECTOR, persisted = true)
    var noDate by localDateTimePersistedProperty(SELECTOR)
    var noDateDefault by localDateTimePersistedProperty(SELECTOR, DEFAULT_TIME_VALUE)

    // list properties:
    @PropertyName("string")
    var strList by stringListPersistedProperty(LIST_SELECTOR, persisted = true)
    var noStrList by stringListPersistedProperty(SELECTOR)
    val noStrListDefault by stringListPersistedProperty(SELECTOR, default = DEFAULT_STRING_LIST_VALUE)

    @PropertyName("integer")
    var intList by intListPersistedProperty(LIST_SELECTOR, persisted = true)
    var noIntList by intListPersistedProperty(SELECTOR)
    val noIntListDefault by intListPersistedProperty(SELECTOR, default = DEFAULT_INTEGER_LIST_VALUE)

    @PropertyName("localDateTime")
    var dateList by localDateTimeListPersistedProperty(LIST_SELECTOR, persisted = true)
    var noDateList by localDateTimeListPersistedProperty(SELECTOR)
    var noDateListDefault by localDateTimeListPersistedProperty(SELECTOR, default = DEFAULT_TIME_LIST_VALUE)
}

class InternalPropertiesHolder : InternalPropertiesFactory(PROPERTIES_FILE_NAME) {

    // single properties:
    @PropertyName("string")
    val str by stringProperty(SELECTOR)
    val noStr by stringProperty(SELECTOR)
    val noStrDefault by stringProperty(SELECTOR, DEFAULT_STRING_VALUE)

    @PropertyName("integer")
    val int by intProperty(SELECTOR)
    val noInt by intProperty(SELECTOR)
    val noIntDefault by intProperty(SELECTOR, DEFAULT_INTEGER_VALUE)

    @PropertyName("boolean")
    val bool by booleanProperty(SELECTOR)
    val noBool by booleanProperty(SELECTOR)
    val noBoolDefault by booleanProperty(SELECTOR, DEFAULT_BOOLEAN_VALUE)
    val boolBadFormatted by booleanProperty(SELECTOR)

    @PropertyName("localDateTime")
    val date by localDateTimeProperty(SELECTOR)
    val noDate by localDateTimeProperty(SELECTOR)
    val noDateDefault by localDateTimeProperty(SELECTOR, DEFAULT_TIME_VALUE)

    // list properties:
    @PropertyName("string")
    val strList by stringListProperty(LIST_SELECTOR)
    val noStrList by stringListProperty(SELECTOR)
    val noStrListDefault by stringListProperty(SELECTOR, default = DEFAULT_STRING_LIST_VALUE)

    @PropertyName("integer")
    val intList by intListProperty(LIST_SELECTOR)
    val noIntList by intListProperty(SELECTOR)
    val noIntListDefault by intListProperty(SELECTOR, default = DEFAULT_INTEGER_LIST_VALUE)

    @PropertyName("localDateTime")
    val dateList by localDateTimeListProperty(LIST_SELECTOR)
    val noDateList by localDateTimeListProperty(SELECTOR)
    val noDateListDefault by localDateTimeListProperty(SELECTOR, default = DEFAULT_TIME_LIST_VALUE)
}

