package com.malencur.file.resources

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import kotlin.test.assertEquals

object ResourceBundleFactoryTest : Spek({
    describe("ResourceBundleFactory") {
        context("FR Locale") {
            val dictionary = DictionaryFr()
            on("getting value") {
                val value = dictionary.greeting
                it("should return it") {
                    assertEquals("Salut", value)
                }
            }
        }
        context("US Locale") {
            val dictionary = DictionaryUs()
            on("getting value") {
                val value = dictionary.greeting
                it("should return it") {
                    assertEquals("Hello", value)
                }
            }
        }
    }
})