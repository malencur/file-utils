package com.malencur.file.resources

class DictionaryUs : ResourceBundleFactory("internationalization/dictionary", "en", "US") {
    @BundlePropertyName("hello")
    val greeting by stringProperty("malencur", "default")
}

class DictionaryFr : ResourceBundleFactory("internationalization/dictionary", "fr", "FR") {
    @BundlePropertyName("hello")
    val greeting by stringProperty("malencur", "default")
}