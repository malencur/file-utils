package com.malencur.file

import java.io.InputStream
import java.io.OutputStream

class FtpClientWatcher(server: String, port: Int, user: String, password: String) :
        FtpClient(server, port, user, password) {

    private var remoteAsInputStream: InputStream? = null
    private var remoteOutputStream: OutputStream? = null

    private val ftpClient: FtpClient = FtpClient(server, port, user, password).apply { load() }

    override fun <T : Any> tryGetRemoteAsInputStream(source: String, processInputStream: (InputStream) -> T): T {
        return ftpClient.tryGetRemoteAsInputStream(source) {
            remoteAsInputStream = it
            processInputStream(it)
        }
    }

    val inputStreamOpen: Boolean
        get() = remoteAsInputStream.isOpen()

    val outputStreamOpen: Boolean
        get() = remoteOutputStream.isOpen()
}