package com.malencur.file

import java.io.File
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.time.LocalDateTime

const val DEFAULT_STRING_VALUE = "defaultName"
const val DEFAULT_BOOLEAN_VALUE = false
const val DEFAULT_INTEGER_VALUE = 100
const val DEFAULT_LONG_VALUE: Long = 10000000000L
val DEFAULT_TIME_VALUE: LocalDateTime = LocalDateTime.parse("2000-01-01T10:00:00")

const val ROOT_DIR = "malencur"
const val SUB_DIR = "tests"
val externalDir
    get() = FileFactory.createDirInUsersHome(ROOT_DIR + File.separator + SUB_DIR)

fun InputStream?.isOpen(): Boolean {
    try {
        val stream = this ?: return false
        stream.available()
        return true
    } catch (e: IOException) {
        return false
    }
}

fun OutputStream?.isOpen(): Boolean {
    try {
        val stream = this ?: return false
        stream.write(1)
        stream.flush()
        return true
    } catch (e: IOException) {
        return false
    }
}