package com.malencur.file

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import java.io.IOException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

object PathParserTest : Spek({
    describe("PathParser") {
        on("empty invalid path creation") {
            it("should throw exception if empty") {
                assertFailsWith(IOException::class, "PathParser init") {
                    PathParser("", FileFactory.Separator("/"))
                }
            }

            it("should throw exception if blank") {
                assertFailsWith(IOException::class, "PathParser init") {
                    PathParser(" ", FileFactory.Separator("/"))
                }
            }
        }
        on("invalid separator value creation") {
            it("should throw exception if empty") {
                assertFailsWith(IOException::class, "PathParser init") {
                    PathParser("wfr", FileFactory.Separator(""))
                }
            }
            it("should throw exception if blank") {
                assertFailsWith(IOException::class, "PathParser init") {
                    PathParser("wfr", FileFactory.Separator(" "))
                }
            }
        }

        context("init with path without separators") {
            on("directory") {
                val parser = PathParser("ddd", FileFactory.Separator("/"))

                it("should return directoryPath") {
                    assertEquals("ddd", parser.directoryPath, "parser.directoryPath")
                }
                it("root should be empty") {
                    assertEquals("", parser.root, "parser.root")
                }
                it("last subDir parent should be root") {
                    assertEquals("", parser.lastSubDirParent, "parser.lastSubDirParent")
                }
                it("subDirs list should have path as item") {
                    assertEquals(listOf("ddd"), parser.subDirs, "parser.subDirs")
                }
                it("should return last subDir") {
                    assertEquals("ddd", parser.lastSubDir, "parser.lastSubDir")
                }
                it("file should be empty") {
                    assertEquals("", parser.file, "parser.file")
                }
            }
            on("directory") {
                val parser = PathParser("test.txt", FileFactory.Separator("/"))

                it("directoryPath should be empty") {
                    assertEquals("", parser.directoryPath, "parser.directoryPath")
                }
                it("root should be empty") {
                    assertEquals("", parser.root, "parser.root")
                }
                it("last subDir parent be empty") {
                    assertEquals("", parser.lastSubDirParent, "parser.lastSubDirParent")
                }
                it("subDirs list should be empty") {
                    assertEquals(emptyList(), parser.subDirs, "parser.subDirs")
                }
                it("last subDir should be null") {
                    assertEquals(null, parser.lastSubDir, "parser.lastSubDir")
                }
                it("should return path as file") {
                    assertEquals("test.txt", parser.file, "parser.file")
                }
            }
        }

        context("one separator") {
            on("with one file at the end") {
                val parser = PathParser("/ddd.txt", FileFactory.Separator("/"))

                it("should return directoryPath") {
                    assertEquals("", parser.directoryPath, "parser.directoryPath")
                }
                it("should return path as root") {
                    assertEquals("", parser.root, "parser.root")
                }
                it("last subDir parent should be root") {
                    assertEquals("", parser.lastSubDirParent, "parser.lastSubDirParent")
                }
                it("subDirs list should be empty") {
                    assertEquals(emptyList(), parser.subDirs, "parser.subDirs")
                }
                it("last subDir should be null") {
                    assertEquals(null, parser.lastSubDir, "parser.lastSubDir")
                }
                it("should return file") {
                    assertEquals("ddd.txt", parser.file, "parser.file")
                }
            }

            on("with one directory at the end") {
                val parser = PathParser("/.ddd", FileFactory.Separator("/"))

                it("should return directoryPath") {
                    assertEquals("/.ddd", parser.directoryPath, "parser.directoryPath")
                }
                it("should return path as root") {
                    assertEquals("/.ddd", parser.root, "parser.root")
                }
                it("last subDir parent should be root") {
                    assertEquals("/.ddd", parser.lastSubDirParent, "parser.lastSubDirParent")
                }
                it("subDirs list should be empty") {
                    assertEquals(emptyList(), parser.subDirs, "parser.subDirs")
                }
                it("last subDir should be null") {
                    assertEquals(null, parser.lastSubDir, "parser.lastSubDir")
                }
                it("file should be empty") {
                    assertEquals("", parser.file, "parser.file")
                }
            }

            on("with directory and file at the end") {
                val parser = PathParser("ddd/test.txt", FileFactory.Separator("/"))

                it("should return directoryPath") {
                    assertEquals("ddd", parser.directoryPath, "parser.directoryPath")
                }
                it("root should be empty") {
                    assertEquals("", parser.root, "parser.root")
                }
                it("last subDir parent be empty") {
                    assertEquals("", parser.lastSubDirParent, "parser.lastSubDirParent")
                }
                it("should return subDirs list") {
                    assertEquals(listOf("ddd"), parser.subDirs, "parser.subDirs")
                }
                it("should return last subDir") {
                    assertEquals("ddd", parser.lastSubDir, "parser.lastSubDir")
                }
                it("should return path as file") {
                    assertEquals("test.txt", parser.file, "parser.file")
                }
            }

            on("with 2 directories") {
                val parser = PathParser("ddd/bbb", FileFactory.Separator("/"))

                it("should return directoryPath") {
                    assertEquals("ddd/bbb", parser.directoryPath, "parser.directoryPath")
                }
                it("root should be empty") {
                    assertEquals("", parser.root, "parser.root")
                }
                it("should return last subDir parent") {
                    assertEquals("ddd", parser.lastSubDirParent, "parser.lastSubDirParent")
                }
                it("should return subDirs list") {
                    assertEquals(listOf("ddd", "bbb"), parser.subDirs, "parser.subDirs")
                }
                it("should return last subDir") {
                    assertEquals("bbb", parser.lastSubDir, "parser.lastSubDir")
                }
                it("file should be empty") {
                    assertEquals("", parser.file, "parser.file")
                }
            }
        }

        context("two separators") {
            on("with root and file at the end") {
                val parser = PathParser("/.ddd/test.txt", FileFactory.Separator("/"))

                it("should return directoryPath") {
                    assertEquals("/.ddd", parser.directoryPath, "parser.directoryPath")
                }
                it("should return root") {
                    assertEquals("/.ddd", parser.root, "parser.root")
                }
                it("last subDir parent should be root") {
                    assertEquals("/.ddd", parser.lastSubDirParent, "parser.lastSubDirParent")
                }
                it("subDirs list should be empty") {
                    assertEquals(emptyList(), parser.subDirs, "parser.subDirs")
                }
                it("last subDir should be null") {
                    assertEquals(null, parser.lastSubDir, "parser.lastSubDir")
                }
                it("should return file") {
                    assertEquals("test.txt", parser.file, "parser.file")
                }
            }

            on("with root and directory at the end") {
                val parser = PathParser("/.ddd/bbb", FileFactory.Separator("/"))

                it("should return directoryPath") {
                    assertEquals("/.ddd/bbb", parser.directoryPath, "parser.directoryPath")
                }
                it("should return root") {
                    assertEquals("/.ddd", parser.root, "parser.root")
                }
                it("last subDir parent should be root") {
                    assertEquals("/.ddd", parser.lastSubDirParent, "parser.lastSubDirParent")
                }
                it("should return subDirs with one item") {
                    assertEquals(listOf("bbb"), parser.subDirs, "parser.subDirs")
                }
                it("should return last subDir") {
                    assertEquals("bbb", parser.lastSubDir, "parser.lastSubDir")
                }
                it("file should be empty") {
                    assertEquals("", parser.file, "parser.file")
                }
            }

            on("without root and file at the end") {
                val parser = PathParser(".ddd/bbb/test.txt", FileFactory.Separator("/"))

                it("should return directoryPath") {
                    assertEquals(".ddd/bbb", parser.directoryPath, "parser.directoryPath")
                }
                it("root should be empty") {
                    assertEquals("", parser.root, "parser.root")
                }
                it("should return last subDir parent") {
                    assertEquals(".ddd", parser.lastSubDirParent, "parser.lastSubDirParent")
                }
                it("should return subDirs") {
                    assertEquals(listOf(".ddd", "bbb"), parser.subDirs, "parser.subDirs")
                }
                it("should return last subDir") {
                    assertEquals("bbb", parser.lastSubDir, "parser.lastSubDir")
                }
                it("should return file") {
                    assertEquals("test.txt", parser.file, "parser.file")
                }
            }

            on("without root and dir at the end") {
                val parser = PathParser(".ddd/bbb/ccc", FileFactory.Separator("/"))

                it("should return directoryPath") {
                    assertEquals(".ddd/bbb/ccc", parser.directoryPath, "parser.directoryPath")
                }
                it("root should be empty") {
                    assertEquals("", parser.root, "parser.root")
                }
                it("should return last subDir parent") {
                    assertEquals(".ddd/bbb", parser.lastSubDirParent, "parser.lastSubDirParent")
                }
                it("should return subDirs") {
                    assertEquals(listOf(".ddd", "bbb", "ccc"), parser.subDirs, "parser.subDirs")
                }
                it("should return last subDir") {
                    assertEquals("ccc", parser.lastSubDir, "parser.lastSubDir")
                }
                it("file should be empty") {
                    assertEquals("", parser.file, "parser.file")
                }
            }
        }

        context("tree separators") {
            on("with root and file at the end") {
                val parser = PathParser("/.ddd/bbb/test.txt", FileFactory.Separator("/"))

                it("should return directoryPath") {
                    assertEquals("/.ddd/bbb", parser.directoryPath, "parser.directoryPath")
                }
                it("should return root") {
                    assertEquals("/.ddd", parser.root, "parser.root")
                }
                it("last subDir parent should be root") {
                    assertEquals("/.ddd", parser.lastSubDirParent, "parser.lastSubDirParent")
                }
                it("should return subDirs with one item") {
                    assertEquals(listOf("bbb"), parser.subDirs, "parser.subDirs")
                }
                it("should return last subDir") {
                    assertEquals("bbb", parser.lastSubDir, "parser.lastSubDir")
                }
                it("should return file") {
                    assertEquals("test.txt", parser.file, "parser.file")
                }
            }

            on("with root and directory at the end") {
                val parser = PathParser("/.ddd/bbb/ccc", FileFactory.Separator("/"))

                it("should return directoryPath") {
                    assertEquals("/.ddd/bbb/ccc", parser.directoryPath, "parser.directoryPath")
                }
                it("should return root") {
                    assertEquals("/.ddd", parser.root, "parser.root")
                }
                it("last subDir parent should be root") {
                    assertEquals("/.ddd/bbb", parser.lastSubDirParent, "parser.lastSubDirParent")
                }
                it("should return subDirs with 2 items") {
                    assertEquals(listOf("bbb", "ccc"), parser.subDirs, "parser.subDirs")
                }
                it("should return last subDir") {
                    assertEquals("ccc", parser.lastSubDir, "parser.lastSubDir")
                }
                it("file should be empty") {
                    assertEquals("", parser.file, "parser.file")
                }
            }

            on("without root and file at the end") {
                val parser = PathParser(".ddd/bbb/ccc/test.txt", FileFactory.Separator("/"))

                it("should return directoryPath") {
                    assertEquals(".ddd/bbb/ccc", parser.directoryPath, "parser.directoryPath")
                }
                it("root should be empty") {
                    assertEquals("", parser.root, "parser.root")
                }
                it("should return last subDir parent") {
                    assertEquals(".ddd/bbb", parser.lastSubDirParent, "parser.lastSubDirParent")
                }
                it("should return subDirs") {
                    assertEquals(listOf(".ddd", "bbb", "ccc"), parser.subDirs, "parser.subDirs")
                }
                it("should return last subDir") {
                    assertEquals("ccc", parser.lastSubDir, "parser.lastSubDir")
                }
                it("should return file") {
                    assertEquals("test.txt", parser.file, "parser.file")
                }
            }

            on("without root and dir at the end") {
                val parser = PathParser(".ddd/bbb/ccc/aaa", FileFactory.Separator("/"))

                it("should return directoryPath") {
                    assertEquals(".ddd/bbb/ccc/aaa", parser.directoryPath, "parser.directoryPath")
                }
                it("root should be empty") {
                    assertEquals("", parser.root, "parser.root")
                }
                it("should return last subDir parent") {
                    assertEquals(".ddd/bbb/ccc", parser.lastSubDirParent, "parser.lastSubDirParent")
                }
                it("should return subDirs") {
                    assertEquals(listOf(".ddd", "bbb", "ccc", "aaa"), parser.subDirs, "parser.subDirs")
                }
                it("should return last subDir") {
                    assertEquals("aaa", parser.lastSubDir, "parser.lastSubDir")
                }
                it("file should be empty") {
                    assertEquals("", parser.file, "parser.file")
                }
            }
        }

        context("four separators") {
            on("with root and file at the end") {
                val parser = PathParser("/.ddd/bbb/ccc/test.txt", FileFactory.Separator("/"))

                it("should return directoryPath") {
                    assertEquals("/.ddd/bbb/ccc", parser.directoryPath, "parser.directoryPath")
                }
                it("should return root") {
                    assertEquals("/.ddd", parser.root, "parser.root")
                }
                it("last subDir parent should be root") {
                    assertEquals("/.ddd/bbb", parser.lastSubDirParent, "parser.lastSubDirParent")
                }
                it("should return subDirs with 2 items") {
                    assertEquals(listOf("bbb", "ccc"), parser.subDirs, "parser.subDirs")
                }
                it("should return last subDir") {
                    assertEquals("ccc", parser.lastSubDir, "parser.lastSubDir")
                }
                it("should return file") {
                    assertEquals("test.txt", parser.file, "parser.file")
                }
            }

            on("with root and directory at the end") {
                val parser = PathParser("/.ddd/bbb/ccc/aaa", FileFactory.Separator("/"))

                it("should return directoryPath") {
                    assertEquals("/.ddd/bbb/ccc/aaa", parser.directoryPath, "parser.directoryPath")
                }
                it("should return root") {
                    assertEquals("/.ddd", parser.root, "parser.root")
                }
                it("last subDir parent should be root") {
                    assertEquals("/.ddd/bbb/ccc", parser.lastSubDirParent, "parser.lastSubDirParent")
                }
                it("should return subDirs with 3 items") {
                    assertEquals(listOf("bbb", "ccc", "aaa"), parser.subDirs, "parser.subDirs")
                }
                it("should return last subDir") {
                    assertEquals("aaa", parser.lastSubDir, "parser.lastSubDir")
                }
                it("file should be empty") {
                    assertEquals("", parser.file, "parser.file")
                }
            }

            on("without root and file at the end") {
                val parser = PathParser(".ddd/bbb/ccc/aaa/test.txt", FileFactory.Separator("/"))

                it("should return directoryPath") {
                    assertEquals(".ddd/bbb/ccc/aaa", parser.directoryPath, "parser.directoryPath")
                }
                it("root should be empty") {
                    assertEquals("", parser.root, "parser.root")
                }
                it("should return last subDir parent") {
                    assertEquals(".ddd/bbb/ccc", parser.lastSubDirParent, "parser.lastSubDirParent")
                }
                it("should return subDirs") {
                    assertEquals(listOf(".ddd", "bbb", "ccc", "aaa"), parser.subDirs, "parser.subDirs")
                }
                it("should return last subDir") {
                    assertEquals("aaa", parser.lastSubDir, "parser.lastSubDir")
                }
                it("should return file") {
                    assertEquals("test.txt", parser.file, "parser.file")
                }
            }

            on("without root and dir at the end") {
                val parser = PathParser(".ddd/bbb/ccc/aaa/eee", FileFactory.Separator("/"))

                it("should return directoryPath") {
                    assertEquals(".ddd/bbb/ccc/aaa/eee", parser.directoryPath, "parser.directoryPath")
                }
                it("root should be empty") {
                    assertEquals("", parser.root, "parser.root")
                }
                it("should return last subDir parent") {
                    assertEquals(".ddd/bbb/ccc/aaa", parser.lastSubDirParent, "parser.lastSubDirParent")
                }
                it("should return subDirs") {
                    assertEquals(listOf(".ddd", "bbb", "ccc", "aaa", "eee"), parser.subDirs, "parser.subDirs")
                }
                it("should return last subDir") {
                    assertEquals("eee", parser.lastSubDir, "parser.lastSubDir")
                }
                it("file should be empty") {
                    assertEquals("", parser.file, "parser.file")
                }
            }
        }
    }
})