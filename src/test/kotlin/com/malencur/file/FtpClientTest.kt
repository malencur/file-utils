package com.malencur.file

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.mockftpserver.fake.FakeFtpServer
import org.mockftpserver.fake.UserAccount
import org.mockftpserver.fake.filesystem.DirectoryEntry
import org.mockftpserver.fake.filesystem.FileEntry
import org.mockftpserver.fake.filesystem.UnixFakeFileSystem
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import kotlin.properties.Delegates
import kotlin.test.*


object FtpClientTest : Spek({
    describe("FtpClient") {

        var fakeServer: FakeFtpServer by Delegates.notNull()
        var ftpClient: FtpClient by Delegates.notNull()

        fun createFakeServer(): FakeFtpServer {
            return FakeFtpServer().apply {
                addUserAccount(UserAccount("user", "password", "/data"))
                UnixFakeFileSystem().apply {
                    add(DirectoryEntry("/data"))
                    add(FileEntry("/data/foobar.txt", "abcdef 1234567890"))
                }.let {
                    fileSystem = it
                }
                serverControlPort = 0
                start()
            }
        }

        fun createFtpClient(fakeServer: FakeFtpServer): FtpClient {
            return FtpClient("localhost", fakeServer.serverControlPort, "user", "password").apply { load() }
        }
        beforeEachTest {
            fakeServer = createFakeServer()
            ftpClient = createFtpClient(fakeServer)
        }

        afterEachTest {
            ftpClient.close()
            fakeServer.stop()
        }

        on("logged in to remote server") {
            it("should be available") {
                assertTrue("expected ftpClient.available to be true, but was false") {
                    ftpClient.available
                }
            }

            it("should say it's connected") {
                assertTrue("expected ftpClient.connected to be true, but was false") {
                    ftpClient.connected
                }
            }
        }

        on("remote server closed session") {
            fakeServer.setCommandHandler("LIST") { _, session ->
                session.close()
            }

            try {
                ftpClient.fileExists("/data/foo.txt")
            } catch (_: IOException) {
            }

            it("should not ba available") {
                assertFalse("expected ftpClient.available to be false, but was true") {
                    ftpClient.available
                }
            }

            it("should say it's not connected") {
                assertFalse("expected ftpClient.connected to be false, but was true") {
                    ftpClient.connected
                }
            }

            it("should try to reconnect") {
                runBlocking {
                    ftpClient.reconnect()
                    delay(2500)

                    assertTrue("expected ftpClient.connected to be true, but was false") {
                        ftpClient.connected
                    }
                }
            }
        }

        on("trying to check if file exist on remote server") {
            it("should return true for existing one") {
                assertTrue("expected ftpClient.fileExists return true, but was false") {
                    ftpClient.fileExists("/data/foobar.txt")
                }
            }

            it("should return false for not existing one") {
                assertFalse("expected ftpClient.fileExists return false, but was true") {
                    ftpClient.fileExists("/data/foobar1.txt")
                }
            }
        }

        on("trying to get list of files inside dir") {
            it("should return one") {
                val listFiles = ftpClient.listFiles("")
                listFiles.let {
                    assertEquals(1, it.size, "listFiles.size")
                    assertEquals(it[0], "foobar.txt", "listFiles.size[0]")
                }
            }
        }

        on("trying to download file") {
            it("should be on the local file system") {
                try {
                    assertTrue("Expected ftpClient.downloadFile to be true, but was false") {
                        ftpClient.downloadFile("/data/foobar.txt", "downloaded_buz.txt")
                    }
                } finally {
                    File("downloaded_buz.txt").run {
                        if (exists()) {
                            delete() // cleanup
                        } else {
                            fail("expected [downloaded_buz.txt] to be on local file system, but was not")
                        }
                    }
                }
            }
        }

        on("trying to download not existing file") {
            it("should not create empty local file") {
                try {
                    assertFalse("Expected ftpClient.downloadFile to be false, but was true") {
                        ftpClient.downloadFile("/data/foobar2.txt", "downloaded_buz.txt")
                    }

                    File("downloaded_buz.txt").run {
                        if (exists()) {
                            delete() // cleanup
                            fail("file [downloaded_buz.txt] exists")
                        }
                    }
                } finally {
                    File("downloaded_buz.txt").run {
                        if (exists()) delete() // cleanup
                    }
                }
            }
        }

        on("trying to upload new file") {
            it("should be on the remote system") {
                val toUpload = fileFromResource("ftp/buz.txt")
                assertTrue("expected ftpClient.uploadFile to return true, but was false") {
                    ftpClient.uploadFile(toUpload, "/data/temp/test/downloaded_buz.txt")
                }
                assertTrue("expected fakeServer.fileSystem.exists to return true, but was false") {
                    fakeServer.fileSystem.exists("/data/temp/test/downloaded_buz.txt")
                }
            }
        }

        on("trying to upload existing file") {
            it("should rewrite it") {

                val bytesFromLocal = bytesFromResource("ftp/buz.txt")

                val toUpload = fileFromResource("ftp/buz.txt")

                assertTrue("expected ftpClient.uploadFile to return true, but was false") {
                    ftpClient.uploadFile(toUpload, "/data/foobar.txt")
                }
                assertTrue("expected fakeServer.fileSystem.exists to return true, but was false") {
                    fakeServer.fileSystem.exists("/data/foobar.txt")
                }

                val bytesFromRemote = ftpClient.tryGetRemoteAsInputStream("/data/foobar.txt") {
                    it.readBytes()
                }

                assertTrue("bytesFromLocal are not equal to bytesFromRemote") {
                    bytesFromLocal.contentEquals(bytesFromRemote)
                }
            }
        }

        on("trying to use InputStream from remote file") {

            it("should allow to construct and return File from it's closure") {

                var stream: InputStream? = null

                val file = ftpClient.tryGetRemoteAsInputStream("/data/foobar.txt") {
                    stream = it
                    it.toFile("downloaded_buz.txt")
                }

                assertTrue("Expected file.exists to be true, but was false") { file.exists() }

                file.delete() // cleanup

                assertFalse("stream.isOpen(): expected to be false, but was true") {
                    stream.isOpen()
                }
            }

            it("should throw an error if there is no such a file on remote server") {
                var stream: InputStream? = null
                assertFailsWith(IOException::class, "ftpClient.tryGetRemoteAsInputStream") {
                    ftpClient.tryGetRemoteAsInputStream("/data/foobar1.txt") { stream = it }
                }
                assertFalse("stream.isOpen(): expected to be false, but was true") {
                    stream.isOpen()
                }
            }

            it("should not allow to return provided InputStream from it's closure") {
                var stream: InputStream? = null
                assertFailsWith(IllegalReturnTypeException::class, "ftpClient.tryGetRemoteAsInputStream { return InputStream }") {
                    ftpClient.tryGetRemoteAsInputStream("/data/foobar.txt") {
                        stream = it
                        it
                    }
                }
                assertFalse("stream.isOpen(): expected to be false, but was true") {
                    stream.isOpen()
                }
            }
        }

        on("trying to use OutputStream from remote file") {

            it("should override existing one") {
                var stream: OutputStream? = null
                val bytesFromLocal = bytesFromResource("ftp/buz.txt")

                ftpClient.tryGetRemoteAsOutputStream("/data/foobar.txt") {
                    stream = it
                    inputStreamFromResource("ftp/buz.txt").copyTo(it)
                }

                val bytesFromRemote = ftpClient.tryGetRemoteAsInputStream("/data/foobar.txt") {
                    it.readBytes()
                }

                assertTrue("bytesFromLocal are not equal to bytesFromRemote") {
                    bytesFromLocal.contentEquals(bytesFromRemote)
                }

                assertFalse("stream.isOpen(): expected to be false, but was true") {
                    stream.isOpen()
                }
            }

            it("should create new one") {
                var stream: OutputStream? = null
                val bytesFromLocal = bytesFromResource("ftp/buz.txt")

                ftpClient.tryGetRemoteAsOutputStream("/data/temp/test/newFile.txt") {
                    stream = it
                    inputStreamFromResource("ftp/buz.txt").copyTo(it)
                }

                assertTrue("expected fakeServer.fileSystem.exists to return true, but was false") {
                    fakeServer.fileSystem.exists("/data/temp/test/newFile.txt")
                }

                val bytesFromRemote = ftpClient.tryGetRemoteAsInputStream("/data/temp/test/newFile.txt") {
                    it.readBytes()
                }

                assertTrue("bytesFromLocal are not equal to bytesFromRemote") {
                    bytesFromLocal.contentEquals(bytesFromRemote)
                }

                assertFalse("stream.isOpen(): expected to be false, but was true") {
                    stream.isOpen()
                }
            }

            it("should create an empty remote file if nothing was written to OutputStream") {

                assertTrue("expected ftpClient.createEmptyFile to return true, but was false") {
                    ftpClient.createEmptyFile("/data/temp/test/emptyFile.txt")
                }

                assertTrue("expected fakeServer.fileSystem.exists to return true, but was false") {
                    fakeServer.fileSystem.exists("/data/temp/test/emptyFile.txt")
                }

                val bytesFromRemote = ftpClient.tryGetRemoteAsInputStream("/data/temp/test/emptyFile.txt") {
                    it.readBytes()
                }

                assertTrue("expected bytesFromRemote to be empty, but was not") { bytesFromRemote.isEmpty() }
            }
        }

        on("trying to create one new directory") {
            it("should create one with absolute path") {

                assertTrue("expected ftpClient.createDirs return true, but was false") {
                    ftpClient.createDir("/data/newDir1")
                }

                assertTrue("expected fakeServer.fileSystem.exists to return true, but was false") {
                    fakeServer.fileSystem.exists("/data/newDir1")
                }
            }

            it("should create one with relative path") {

                assertTrue("expected ftpClient.createDirs return true, but was false") {
                    ftpClient.createDir("newDir2")
                }

                assertTrue("expected fakeServer.fileSystem.exists to return true, but was false") {
                    fakeServer.fileSystem.exists("/data/newDir2")
                }
            }

            it("should create nested one with absolute path") {

                assertTrue("expected ftpClient.createDirs return true, but was false") {
                    ftpClient.createDir("/data/newDir1/newDir11")
                }

                assertTrue("expected fakeServer.fileSystem.exists to return true, but was false") {
                    fakeServer.fileSystem.exists("/data/newDir1/newDir11")
                }
            }

            it("should create nested one with relative path") {

                assertTrue("expected ftpClient.createDirs return true, but was false") {
                    ftpClient.createDir("newDir2/newDir22")
                }

                assertTrue("expected fakeServer.fileSystem.exists to return true, but was false") {
                    fakeServer.fileSystem.exists("/data/newDir2/newDir22")
                }
            }

            it("should create many with existing root") {

                assertTrue("expected ftpClient.createDirs return true, but was false") {
                    ftpClient.createDirs("/data", "newDir3", "newDir33", "newDir333")
                }

                assertTrue("expected fakeServer.fileSystem.exists to return true, but was false") {
                    fakeServer.fileSystem.exists("/data/newDir3/newDir33/newDir333")
                }
            }

            it("should not create many with not existing root") {

                assertFalse("expected ftpClient.createDirs return false, but was true") {
                    ftpClient.createDirs("/data/newDir4", "newDir44", "newDir444", "newDir4444")
                }

                assertFalse("expected fakeServer.fileSystem.exists to return false, but was true") {
                    fakeServer.fileSystem.exists("/data/newDir4/newDir44/newDir444/newDir4444")
                }
            }
        }

        context("Creating duplicates") {
            on("on one directory with root") {
                it("should have only one file entry in the system at the beginning") {
                    val files = fakeServer.fileSystem.listFiles("/data")
                    assertEquals(1, files.size, "files.size")
                }
                it("should create new one") {
                    assertTrue("expected ftpClient.createDir return true, but was false") {
                        ftpClient.createDir("/data/newDir1")
                    }
                }
                it("should swallow duplicate creation and return true") {
                    assertTrue("expected ftpClient.createDirs return true, but was false") {
                        ftpClient.createDir("/data/newDir1")
                    }
                }
                it("should contain new directory in the file system") {
                    assertTrue("expected fakeServer.fileSystem.exists to return true, but was false") {
                        fakeServer.fileSystem.exists("/data/newDir1")
                    }
                }
                it("should have only 2 entries") {
                    val files = fakeServer.fileSystem.listFiles("/data")
                    assertEquals(2, files.size, "files.size for [/data]")
                }
            }

            on("on one directory without root") {
                it("should have only one file entry in the system at the beginning") {
                    val files = fakeServer.fileSystem.listFiles("/data")
                    assertEquals(1, files.size, "files.size")
                }
                it("should create new one") {
                    assertTrue("expected ftpClient.createDir return true, but was false") {
                        ftpClient.createDir("newDir1")
                    }
                }
                it("should swallow duplicate creation and return true") {
                    assertTrue("expected ftpClient.createDirs return true, but was false") {
                        ftpClient.createDir("newDir1")
                    }
                }
                it("should contain new directory in the file system") {
                    assertTrue("expected fakeServer.fileSystem.exists to return true, but was false") {
                        fakeServer.fileSystem.exists("/data/newDir1")
                    }
                }
                it("should have only 2 entries") {
                    val files = fakeServer.fileSystem.listFiles("/data")
                    assertEquals(2, files.size, "files.size for [/data]")
                }
            }

            on("many directories with root") {
                it("should have only one file entry in the system at the beginning") {
                    val files = fakeServer.fileSystem.listFiles("/data")
                    assertEquals(1, files.size, "files.size")
                }
                it("should create many new ones") {
                    assertTrue("expected ftpClient.createDirs return true, but was false") {
                        ftpClient.createDirs("/data", "newDir1", "newDir11", "newDir111")
                    }
                }
                it("should swallow duplicate creation and return true") {
                    assertTrue("expected ftpClient.createDirs return true, but was false") {
                        ftpClient.createDirs("/data", "newDir1", "newDir11", "newDir111")
                    }
                }
                it("should contain new directory tree in the file system") {
                    assertTrue("expected fakeServer.fileSystem.exists to return true, but was false") {
                        fakeServer.fileSystem.exists("/data/newDir1/newDir11/newDir111")
                    }
                }
                it("should not have duplicates in the file system") {
                    assertEquals(2, fakeServer.fileSystem.listFiles("/data").size, "files.size for [/data]")
                    assertEquals(1, fakeServer.fileSystem.listFiles("/data/newDir1").size, "files.size for [/data/newDir1]")
                    assertEquals(1, fakeServer.fileSystem.listFiles("/data/newDir1/newDir11").size, "files.size for [/data/newDir1/newDir11]")
                }
            }

            on("many directories without root") {
                it("should have only one file entry in the system at the beginning") {
                    val files = fakeServer.fileSystem.listFiles("/data")
                    assertEquals(1, files.size, "files.size")
                }
                it("should create many new ones") {
                    assertTrue("expected ftpClient.createDirs return true, but was false") {
                        ftpClient.createDirs("", "newDir1", "newDir11", "newDir111")
                    }
                }
                it("should swallow duplicate creation and return true") {
                    assertTrue("expected ftpClient.createDirs return true, but was false") {
                        ftpClient.createDirs("", "newDir1", "newDir11", "newDir111")
                    }
                }
                it("should contain new directory tree in the file system") {
                    assertTrue("expected fakeServer.fileSystem.exists to return true, but was false") {
                        fakeServer.fileSystem.exists("/data/newDir1/newDir11/newDir111")
                    }
                }
                it("should not have duplicates in the file system") {
                    assertEquals(2, fakeServer.fileSystem.listFiles("/data").size, "files.size for [/data]")
                    assertEquals(1, fakeServer.fileSystem.listFiles("/data/newDir1").size, "files.size for [/data/newDir1]")
                    assertEquals(1, fakeServer.fileSystem.listFiles("/data/newDir1/newDir11").size, "files.size for [/data/newDir1/newDir11]")
                }
            }

            on("trying to prepare many directories with root") {
                it("should have only one file entry in the system at the beginning") {
                    val files = fakeServer.fileSystem.listFiles("/data")
                    assertEquals(1, files.size, "files.size")
                }
                it("should prepare first time without exceptions") {
                    ftpClient.tryPrepareDirs("/data/newDir1/newDir11/newDir111/test.txt")
                }
                it("should swallow duplicate creation during second time") {
                    ftpClient.tryPrepareDirs("/data/newDir1/newDir11/newDir111/test.txt")
                }
                it("should contain new directory tree without file at the end") {
                    assertTrue("expected fakeServer.fileSystem.exists [/data/newDir1/newDir11] to return true, but was false") {
                        fakeServer.fileSystem.exists("/data/newDir1/newDir11/newDir111")
                    }
                    assertFalse("expected fakeServer.fileSystem.exists [/data/newDir1/newDir11/newDir111/test.txt] to return false, but was true") {
                        fakeServer.fileSystem.exists("/data/newDir1/newDir11/newDir111/test.txt")
                    }
                }
                it("should not have duplicates in the file system") {
                    assertEquals(2, fakeServer.fileSystem.listFiles("/data").size, "files.size for [/data]")
                    assertEquals(1, fakeServer.fileSystem.listFiles("/data/newDir1").size, "files.size for [/data/newDir1]")
                    assertEquals(1, fakeServer.fileSystem.listFiles("/data/newDir1/newDir11").size, "files.size for [/data/newDir1/newDir11]")
                    assertEquals(0, fakeServer.fileSystem.listFiles("/data/newDir1/newDir11/newDir111").size, "files.size for [/data/newDir1/newDir11/newDir111]")
                }
            }

            on("trying to prepare many directories without root") {
                it("should have only one file entry in the system at the beginning") {
                    val files = fakeServer.fileSystem.listFiles("/data")
                    assertEquals(1, files.size, "files.size")
                }
                it("should prepare first time without exceptions") {
                    ftpClient.tryPrepareDirs("newDir1/newDir11/newDir111/test.txt")
                }
                it("should swallow duplicate creation during second time") {
                    ftpClient.tryPrepareDirs("newDir1/newDir11/newDir111/test.txt")
                }
                it("should contain new directory tree without file at the end") {
                    assertTrue("expected fakeServer.fileSystem.exists [/data/newDir1/newDir11] to return true, but was false") {
                        fakeServer.fileSystem.exists("/data/newDir1/newDir11/newDir111")
                    }
                    assertFalse("expected fakeServer.fileSystem.exists [/data/newDir1/newDir11/newDir111/test.txt] to return false, but was true") {
                        fakeServer.fileSystem.exists("/data/newDir1/newDir11/newDir111/test.txt")
                    }
                }
                it("should not have duplicates in the file system") {
                    assertEquals(2, fakeServer.fileSystem.listFiles("/data").size, "files.size for [/data]")
                    assertEquals(1, fakeServer.fileSystem.listFiles("/data/newDir1").size, "files.size for [/data/newDir1]")
                    assertEquals(1, fakeServer.fileSystem.listFiles("/data/newDir1/newDir11").size, "files.size for [/data/newDir1/newDir11]")
                    assertEquals(0, fakeServer.fileSystem.listFiles("/data/newDir1/newDir11/newDir111").size, "files.size for [/data/newDir1/newDir11/newDir111]")
                }
            }
        }

        context("Connection closed") {
            on("trying to create one directory") {
                fakeServer.setCommandHandler("MKD") { _, session ->
                    session.close()
                }

                it("should return false") {
                    assertFalse("expected ftpClient.createDir return false, but was true") {
                        ftpClient.createDir("/data/newDir")
                    }
                }

            }

            on("trying to create many directories") {
                fakeServer.setCommandHandler("MKD") { _, session ->
                    session.close()
                }

                it("should return false") {
                    assertFalse("expected ftpClient.createDirs return false, but was true") {
                        ftpClient.createDirs("/data", "newDir1", "newDir11", "newDir111")
                    }
                }

            }
        }
    }
})