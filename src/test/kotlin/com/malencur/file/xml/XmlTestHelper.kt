package com.malencur.file.xml

import com.malencur.file.*
import com.malencur.file.xml.property.*

internal const val BROKEN_XML_FILE_NAME = "broken.xml"
internal const val XML_FILE_NAME = "terminals.xml"
internal const val XML_FILE_PROPERTIES_NAME = "properties.xml"

@XmlNode("terminals")
internal class TerminalsHolder(context: XmlElementContext) : XmlNodeElementHolder(context), XmlElementList.Listener {

    var elementsAdded = 0
    var elementsRemoved = 0
    var elementReplaced = false

    override fun elementsAdded(number: Int) {
        elementsAdded += number
    }

    override fun elementsRemoved(number: Int) {
        elementsRemoved += number
    }

    override fun elementReplaced() {
        elementReplaced = true
    }

    val terminals by list(Terminal::class, this)
}

internal class Terminal(context: XmlElementContext) : XmlNodeElementHolder(context) {
    var id by XmlElementIntegerAttribute(element)
    @XmlAttribute("levelindex")
    var levelIndex by XmlElementIntegerAttribute(element)
    var state by XmlElementIntegerAttribute(element)
    @XmlAttribute("terminalnumber")
    var terminalNumber by XmlElementIntegerAttribute(element)
    var visible by XmlElementBooleanAttribute(element)

    val clientAssigned by lazy { getSingletonChild(ClientAssigned::class) }
    val previousClientsHolder by lazy { getSingletonChild(PreviousClientsHolder::class) }
}

@XmlNode("clientassigned")
internal class ClientAssigned(context: XmlElementContext) : XmlNodeElementHolder(context) {
    var number by XmlElementIntegerTextContent(element)
}

@XmlNode("previousclients")
internal class PreviousClientsHolder(context: XmlElementContext) : XmlNodeElementHolder(context) {
    val clients by XmlElementListLazyProperty(
            document,
            element,
            getChildrenNodeList("client"),
            ClientNode::class
    )
}

@XmlNode("client")
internal class ClientNode(context: XmlElementContext) : XmlNodeElementHolder(context) {
    var number by XmlElementIntegerTextContent(element)
}

internal class ExpectedException : RuntimeException()

internal class DocumentFactory : XmlDocumentFromTemplateFileFactory(
        XML_FILE_NAME, externalDir, onDocumentParseError = { throw ExpectedException() }
) {

    val terminalsHolder by lazy { getRootElementSingletonChild(TerminalsHolder::class) }
}

internal class FtpDocumentFactory(ftpClient: FtpClient) : FtpXmlDocumentFromTemplateFileFactory(
        ftpClient, XML_FILE_NAME, "/data/temp/test", onDocumentParseError = { throw ExpectedException() }
) {
    val terminalsHolder by lazy { getRootElementSingletonChild(TerminalsHolder::class) }
}

internal class NewDocumentFactory : XmlDocumentFactory(
        XML_FILE_NAME, externalDir, init = false, onDocumentParseError = { throw ExpectedException() }
) {

    val terminalsHolder by lazy { createRootElementSingletonChild(TerminalsHolder::class) }

    init {
        setNewDocument("root")
        initDocument()
    }
}

internal class BrokenDocumentWithListener : XmlDocumentFromTemplateFileFactory(
        BROKEN_XML_FILE_NAME, externalDir, onDocumentParseError = { throw ExpectedException() }
)

internal class BrokenDocumentWithoutListener : XmlDocumentFromTemplateFileFactory(BROKEN_XML_FILE_NAME, externalDir)

@XmlNode("noneexisting")
internal class NoneExisting(context: XmlElementContext) : XmlNodeElementHolder(context) {
    val attribute by stringAttribute()
}

@XmlNode("stringproperty")
internal class StringProperty(context: XmlElementContext) : XmlNodeElementHolder(context) {
    val attribute by stringAttribute()
    val content by stringContent()
}

@XmlNode("booleanproperty")
internal class BooleanProperty(context: XmlElementContext) : XmlNodeElementHolder(context) {
    val attribute by boolAttribute()
    val content by boolContent()
}

@XmlNode("integerproperty")
internal class IntegerProperty(context: XmlElementContext) : XmlNodeElementHolder(context) {
    val attribute by intAttribute()
    val content by intContent()
}

@XmlNode("longproperty")
internal class LongProperty(context: XmlElementContext) : XmlNodeElementHolder(context) {
    val attribute by longAttribute()
    val content by longContent()
}

@XmlNode("dateproperty")
internal class DateProperty(context: XmlElementContext) : XmlNodeElementHolder(context) {
    val attribute by dateAttribute()
    val content by dateContent()
}

@XmlNode("stringnoproperties")
internal class StringNoProperties(context: XmlElementContext) : XmlNodeElementHolder(context) {
    val attributeWithoutDefault by stringAttribute()
    val attributeWithDefault by stringAttribute(DEFAULT_STRING_VALUE)

    val contentWithoutDefault by stringContent()
    val contentWithDefault by stringContent(DEFAULT_STRING_VALUE)
}

@XmlNode("booleannoproperties")
internal class BooleanNoProperties(context: XmlElementContext) : XmlNodeElementHolder(context) {
    val attributeWithoutDefault by boolAttribute()
    val attributeWithDefault by boolAttribute(DEFAULT_BOOLEAN_VALUE)

    val contentWithoutDefault by boolContent()
    val contentWithDefault by boolContent(DEFAULT_BOOLEAN_VALUE)
}

@XmlNode("integernoproperties")
internal class IntegerNoProperties(context: XmlElementContext) : XmlNodeElementHolder(context) {
    val attributeWithoutDefault by intAttribute()
    val attributeWithDefault by intAttribute(DEFAULT_INTEGER_VALUE)

    val contentWithoutDefault by intContent()
    val contentWithDefault by intContent(DEFAULT_INTEGER_VALUE)
}

@XmlNode("longnoproperties")
internal class LongNoProperties(context: XmlElementContext) : XmlNodeElementHolder(context) {
    val attributeWithoutDefault by longAttribute()
    val attributeWithDefault by longAttribute(DEFAULT_LONG_VALUE)

    val contentWithoutDefault by longContent()
    val contentWithDefault by longContent(DEFAULT_LONG_VALUE)
}

@XmlNode("datenoproperties")
internal class DateNoProperties(context: XmlElementContext) : XmlNodeElementHolder(context) {
    val attributeWithoutDefault by dateAttribute()
    val attributeWithDefault by dateAttribute(DEFAULT_TIME_VALUE)

    val contentWithoutDefault by dateContent()
    val contentWithDefault by dateContent(DEFAULT_TIME_VALUE)
}

internal class DocumentPropertiesFactory : XmlDocumentFromTemplateFileFactory(
        XML_FILE_PROPERTIES_NAME, externalDir, onDocumentParseError = { throw ExpectedException() }
) {
    val noneExistingNode by lazy { getRootElementSingletonChild(NoneExisting::class) }

    val stringProperty by lazy { getRootElementSingletonChild(StringProperty::class) }

    val booleanProperty by lazy { getRootElementSingletonChild(BooleanProperty::class) }

    val integerProperty by lazy { getRootElementSingletonChild(IntegerProperty::class) }

    val longProperty by lazy { getRootElementSingletonChild(LongProperty::class) }

    val dateTimeProperty by lazy { getRootElementSingletonChild(DateProperty::class) }

    val stringNoProperties by lazy { getRootElementSingletonChild(StringNoProperties::class) }

    val booleanNoProperties by lazy { getRootElementSingletonChild(BooleanNoProperties::class) }

    val integerNoProperties by lazy { getRootElementSingletonChild(IntegerNoProperties::class) }

    val longNoProperties by lazy { getRootElementSingletonChild(LongNoProperties::class) }

    val dateTimeNoProperties by lazy { getRootElementSingletonChild(DateNoProperties::class) }
}