package com.malencur.file.xml

import com.malencur.file.FtpClient
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.mockftpserver.fake.FakeFtpServer
import org.mockftpserver.fake.UserAccount
import org.mockftpserver.fake.filesystem.DirectoryEntry
import org.mockftpserver.fake.filesystem.UnixFakeFileSystem
import kotlin.properties.Delegates
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class FtpXmlDocumentFromTemplateFileFactoryTest : Spek({

    describe("FtpXmlDocumentFromTemplateFileFactory") {
        var fakeServer: FakeFtpServer by Delegates.notNull()
        var ftpClient: FtpClient by Delegates.notNull()
        var factory: FtpDocumentFactory by Delegates.notNull()

        fun createFakeServer(): FakeFtpServer {
            return FakeFtpServer().apply {
                addUserAccount(UserAccount("user", "password", "/data"))
                UnixFakeFileSystem().apply {
                    add(DirectoryEntry("/data"))
                }.let {
                    fileSystem = it
                }
                serverControlPort = 0
                start()
            }
        }

        fun createFtpClient(fakeServer: FakeFtpServer): FtpClient {
            return FtpClient("localhost", fakeServer.serverControlPort, "user", "password").apply { load() }
        }

        beforeEachTest {
            fakeServer = createFakeServer()
            ftpClient = createFtpClient(fakeServer)
            factory = FtpDocumentFactory(ftpClient)
        }

        afterEachTest {
            ftpClient.close()
            fakeServer.stop()
        }

        on("document creation") {
            it("should parse content") {
                assertEquals(5, factory.terminalsHolder.terminals.size, "factory.terminalsHolder.terminals")
            }

            it("should contain the file on remote server") {
                assertTrue("expected fakeServer.fileSystem.exists to return true, but was false") {
                    fakeServer.fileSystem.exists("/data/temp/test/terminals.xml")
                }
            }
        }

        on("document change") {
            val terminal = factory.terminalsHolder.createNewListItemChild(Terminal::class)
            terminal.id = 5
            terminal.levelIndex = 0
            terminal.visible = true

            it("should save changes") {
                assertTrue("expected factory.trySaveDocument to return true, but was false") {
                    factory.trySaveDocument()
                }
            }
        }

        on("document change with connection closed") {

            fakeServer.setCommandHandler("LIST") { _, session ->
                session.close()
            }

            val terminal = factory.terminalsHolder.createNewListItemChild(Terminal::class)
            terminal.id = 5
            terminal.levelIndex = 0
            terminal.visible = true

            it("should say changes not saved") {
                assertFalse("expected factory.trySaveDocument to return false, but was true") {
                    factory.trySaveDocument()
                }
            }
        }
    }
})