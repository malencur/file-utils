package com.malencur.file.xml

import com.malencur.file.FileFactory
import com.malencur.file.externalDir
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import java.io.File
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

object XmlLazyListObjectTest: Spek({
    describe("XmlLazyListObjectTest") {
        on("getting list object") {
            FileFactory.deleteFile(externalDir + File.separator + XML_FILE_NAME)
            val doc = DocumentFactory()
            val terminals = doc.terminalsHolder.terminals
            it("should return it") {
                assertNotNull(terminals)
                assertEquals(5, terminals.size)
            }
        }
    }
})