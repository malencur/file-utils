package com.malencur.file.xml.property

import com.malencur.file.FileFactory
import com.malencur.file.externalDir
import com.malencur.file.xml.*
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import java.io.File
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse
import kotlin.test.assertTrue

object XmlElementListTest : Spek({
    describe("XmlElementList") {
        FileFactory.deleteFile(externalDir + File.separator + XML_FILE_NAME)
        context("one element") {
            on("removing") {
                val documentFactory = DocumentFactory()
                val terminalsHolder = documentFactory.terminalsHolder
                val terminals = terminalsHolder.terminals

                terminals.remove(terminals[2])

                it("should state that element was deleted") {
                    assertEquals(4, terminals.size)
                    assertEquals(1, terminalsHolder.elementsRemoved)
                }
            }
            on("removing at index") {
                val documentFactory = DocumentFactory()
                val terminalsHolder = documentFactory.terminalsHolder
                val terminals = terminalsHolder.terminals

                val removed = terminals.removeAt(0)

                it("should state that element was deleted") {
                    assertEquals(4, terminals.size)
                    assertEquals(1, terminalsHolder.elementsRemoved)
                    assertFalse { terminals.contains(removed) }
                }
            }
            on("adding") {
                val documentFactory = DocumentFactory()
                val terminalsHolder = documentFactory.terminalsHolder
                val terminals = terminalsHolder.terminals

                val terminal = Terminal(XmlElementContext(documentFactory.document, terminalsHolder.element, "terminal"))

                terminals.add(terminal)

                it("should state that element was added") {
                    assertEquals(6, terminals.size)
                    assertEquals(1, terminalsHolder.elementsAdded)
                }
            }
            on("adding at index") {
                val documentFactory = DocumentFactory()
                val terminalsHolder = documentFactory.terminalsHolder
                val terminals = terminalsHolder.terminals

                val terminal = Terminal(XmlElementContext(documentFactory.document, terminalsHolder.element, "terminal"))

                terminals.add(0, terminal)

                it("should state that element was added") {
                    assertEquals(6, terminals.size)
                    assertEquals(1, terminalsHolder.elementsAdded)
                }
            }
            on("replacing") {
                val documentFactory = DocumentFactory()
                val terminalsHolder = documentFactory.terminalsHolder
                val terminals = terminalsHolder.terminals

                val terminal = Terminal(XmlElementContext(documentFactory.document, terminalsHolder.element, "terminal"))

                val replaced = terminals.set(0, terminal)

                it("should state correct number of replaced elements") {
                    assertFalse(terminals.contains(replaced))
                    assertTrue(terminalsHolder.elementReplaced)
                }
            }
        }
        context("collection of elements") {
            on("removing") {
                val documentFactory = DocumentFactory()
                val terminalsHolder = documentFactory.terminalsHolder
                val terminals = terminalsHolder.terminals

                val toBeDeleted = mutableListOf<Terminal>()
                toBeDeleted.add(terminals[0])
                toBeDeleted.add(terminals[1])
                toBeDeleted.add(terminals[2])

                terminals.removeAll(toBeDeleted)

                it("should state correct` number of deleted elements") {
                    assertEquals(2, terminals.size)
                    assertEquals(3, terminalsHolder.elementsRemoved)
                }
            }
            on("adding forbidden type item") {
                FileFactory.deleteFile(externalDir + File.separator + XML_FILE_NAME)
                val documentFactory = NewDocumentFactory()
                val terminalsHolder = documentFactory.terminalsHolder

                it("should throw an exception") {
                    assertFailsWith(IllegalArgumentException::class) {
                        terminalsHolder.createNewListItemChild(ClientAssigned::class)
                    }
                }
            }
            on("adding") {
                FileFactory.deleteFile(externalDir + File.separator + XML_FILE_NAME)
                val documentFactory = NewDocumentFactory()
                var terminalsHolder = documentFactory.terminalsHolder

                for (i in 0..2) {
                    terminalsHolder.createNewListItemChild(Terminal::class)
                }

                val actual = terminalsHolder.elementsAdded
                it("should state that element was added") {
                    assertEquals(3, actual)
                }

                documentFactory.saveDocument()

                val docFactory = DocumentFactory()
                terminalsHolder = docFactory.terminalsHolder
                val terminals = terminalsHolder.terminals

                it("should return new items collection after document recreated") {
                    assertEquals(3, terminals.size)
                }
                FileFactory.deleteFile(externalDir + File.separator + XML_FILE_NAME)
            }
            on("adding at index") {
                var documentFactory = DocumentFactory()
                var terminalsHolder = documentFactory.terminalsHolder

                for (i in 0..2) {
                    terminalsHolder.createNewListItemChild(Terminal::class)
                }

                documentFactory.saveDocument()

                val actual = terminalsHolder.elementsAdded
                it("should state that element was added") {
                    assertEquals(3, actual)
                }

                documentFactory = DocumentFactory()
                terminalsHolder = documentFactory.terminalsHolder
                val terminals = terminalsHolder.terminals

                it("should return new items collection after document recreated") {
                    assertEquals(8, terminals.size)
                }
                FileFactory.deleteFile(externalDir + File.separator + XML_FILE_NAME)
            }
            on("clearing list") {
                val documentFactory = DocumentFactory()
                val terminalsHolder = documentFactory.terminalsHolder
                val terminals = terminalsHolder.terminals

                terminals.clear()

                it("should state that all elements have been deleted") {
                    assertEquals(5, terminalsHolder.elementsRemoved)
                }
            }
            on("retaining") {
                val documentFactory = DocumentFactory()
                val terminalsHolder = documentFactory.terminalsHolder
                val terminals = terminalsHolder.terminals

                val toBeRetained = mutableListOf<Terminal>()
                toBeRetained.add(terminals[0])
                toBeRetained.add(terminals[1])

                terminals.retainAll(toBeRetained)

                it("should state correct number of retained elements") {
                    assertEquals(2, terminals.size)
                    assertEquals(3, terminalsHolder.elementsRemoved)
                }
            }
        }
    }
})
