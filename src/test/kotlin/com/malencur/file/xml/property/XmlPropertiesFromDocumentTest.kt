package com.malencur.file.xml.property

import com.malencur.file.*
import com.malencur.file.xml.DocumentPropertiesFactory
import com.malencur.file.xml.XML_FILE_PROPERTIES_NAME
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import java.io.File
import java.io.IOException
import java.time.LocalDateTime
import kotlin.test.*

class XmlPropertiesFromDocumentTest: Spek({
    describe("XmlPropertiesFromDocumentTest") {
        FileFactory.deleteFile(externalDir + File.separator + XML_FILE_PROPERTIES_NAME)
        val properties = DocumentPropertiesFactory()
        on("getting non existing node") {
            it("should throw exception") {
                assertFailsWith(ExceptionInInitializerError::class){
                    properties.noneExistingNode
                }
            }
        }
        context("string"){
            on("getting attribute") {
                val value = properties.stringProperty.attribute
                it("should return it") {
                    assertNotNull(value)
                    assertEquals("commercial1", value)
                }
            }
            on("getting textContent") {
                val value = properties.stringProperty.content
                it("should return it") {
                    assertNotNull(value)
                    assertEquals("C:\\Users\\Public\\eloch\\videos\\test.mp4", value)
                }
            }
            on("getting non existing attribute with default") {
                val value = properties.stringNoProperties.attributeWithDefault
                it("should return default") {
                    assertNotNull(value)
                    assertEquals(DEFAULT_STRING_VALUE, value)
                }
            }
            on("getting non existing textContent with default") {
                val value = properties.stringNoProperties.contentWithDefault
                it("should return default") {
                    assertNotNull(value)
                    assertEquals(DEFAULT_STRING_VALUE, value)
                }
            }
            on("getting non existing attribute without default") {
                it("should throw exception") {
                    assertFailsWith(IOException::class){
                        properties.stringNoProperties.attributeWithoutDefault
                    }
                }
            }
            on("getting non existing textContent without default") {
                it("should throw exception") {
                    assertFailsWith(IOException::class){
                        properties.stringNoProperties.contentWithoutDefault
                    }
                }
            }
        }
        context("boolean"){
            on("getting attribute") {
                val value = properties.booleanProperty.attribute
                it("should return it") {
                    assertNotNull(value)
                    assertFalse(value)
                }
            }
            on("getting textContent") {
                val value = properties.booleanProperty.content
                it("should return it") {
                    assertNotNull(value)
                    assertTrue(value)
                }
            }
            on("getting non existing attribute with default") {
                val value = properties.booleanNoProperties.attributeWithDefault
                it("should return default") {
                    assertNotNull(value)
                    assertEquals(DEFAULT_BOOLEAN_VALUE, value)
                }
            }
            on("getting non existing textContent with default") {
                val value = properties.booleanNoProperties.contentWithDefault
                it("should return default") {
                    assertNotNull(value)
                    assertEquals(DEFAULT_BOOLEAN_VALUE, value)
                }
            }
            on("getting non existing attribute without default") {
                it("should throw exception") {
                    assertFailsWith(IOException::class){
                        properties.booleanNoProperties.attributeWithoutDefault
                    }
                }
            }
            on("getting non existing textContent without default") {
                it("should throw exception") {
                    assertFailsWith(IOException::class){
                        properties.booleanNoProperties.contentWithoutDefault
                    }
                }
            }
        }
        context("integer"){
            on("getting attribute") {
                val value = properties.integerProperty.attribute
                it("should return it") {
                    assertNotNull(value)
                    assertEquals(2, value)
                }
            }
            on("getting textContent") {
                val value = properties.integerProperty.content
                it("should return it") {
                    assertNotNull(value)
                    assertEquals(10, value)
                }
            }
            on("getting non existing attribute with default") {
                val value = properties.integerNoProperties.attributeWithDefault
                it("should return default") {
                    assertNotNull(value)
                    assertEquals(DEFAULT_INTEGER_VALUE, value)
                }
            }
            on("getting non existing textContent with default") {
                val value = properties.integerNoProperties.contentWithDefault
                it("should return default") {
                    assertNotNull(value)
                    assertEquals(DEFAULT_INTEGER_VALUE, value)
                }
            }
            on("getting non existing attribute without default") {
                it("should throw exception") {
                    assertFailsWith(IOException::class){
                        properties.integerNoProperties.attributeWithoutDefault
                    }
                }
            }
            on("getting non existing textContent without default") {
                it("should throw exception") {
                    assertFailsWith(IOException::class){
                        properties.integerNoProperties.contentWithoutDefault
                    }
                }
            }
        }
        context("long") {
            val expected = 10000000001L
            on("getting attribute") {
                val value = properties.longProperty.attribute
                it("should return it") {
                    assertNotNull(value)
                    assertEquals(expected, value)
                }
            }
            on("getting textContent") {
                val value = properties.longProperty.content
                it("should return it") {
                    assertNotNull(value)
                    assertEquals(expected, value)
                }
            }
            on("getting non existing attribute with default") {
                val value = properties.longNoProperties.attributeWithDefault
                it("should return default") {
                    assertNotNull(value)
                    assertEquals(DEFAULT_LONG_VALUE, value)
                }
            }
            on("getting non existing textContent with default") {
                val value = properties.longNoProperties.contentWithDefault
                it("should return default") {
                    assertNotNull(value)
                    assertEquals(DEFAULT_LONG_VALUE, value)
                }
            }
            on("getting non existing attribute without default") {
                it("should throw exception") {
                    assertFailsWith(IOException::class) {
                        properties.longNoProperties.attributeWithoutDefault
                    }
                }
            }
            on("getting non existing textContent without default") {
                it("should throw exception") {
                    assertFailsWith(IOException::class) {
                        properties.integerNoProperties.contentWithoutDefault
                    }
                }
            }
        }
        context("localDateTime"){
            val excepted = LocalDateTime.parse("2018-08-18T12:00:00")
            on("getting attribute") {
                val value = properties.dateTimeProperty.attribute
                it("should return it") {
                    assertNotNull(value)
                    assertEquals(excepted, value)
                }
            }
            on("getting textContent") {
                val value = properties.dateTimeProperty.content
                it("should return it") {
                    assertNotNull(value)
                    assertEquals(excepted, value)
                }
            }
            on("getting non existing attribute with default") {
                val value = properties.dateTimeNoProperties.attributeWithDefault
                it("should return default") {
                    assertNotNull(value)
                    assertEquals(DEFAULT_TIME_VALUE, value)
                }
            }
            on("getting non existing textContent with default") {
                val value = properties.dateTimeNoProperties.contentWithDefault
                it("should return default") {
                    assertNotNull(value)
                    assertEquals(DEFAULT_TIME_VALUE, value)
                }
            }
            on("getting non existing attribute without default") {
                it("should throw exception") {
                    assertFailsWith(IOException::class){
                        properties.dateTimeNoProperties.attributeWithoutDefault
                    }
                }
            }
            on("getting non existing textContent without default") {
                it("should throw exception") {
                    assertFailsWith(IOException::class){
                        properties.dateTimeNoProperties.contentWithoutDefault
                    }
                }
            }
        }
    }
})