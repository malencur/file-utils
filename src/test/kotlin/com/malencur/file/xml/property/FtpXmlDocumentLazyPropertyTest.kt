package com.malencur.file.xml.property

import com.malencur.file.FtpClient
import com.malencur.file.xml.DocumentFactory
import org.apache.commons.net.ftp.FTPConnectionClosedException
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.mockftpserver.fake.FakeFtpServer
import org.mockftpserver.fake.UserAccount
import org.mockftpserver.fake.filesystem.DirectoryEntry
import org.mockftpserver.fake.filesystem.FileEntry
import org.mockftpserver.fake.filesystem.UnixFakeFileSystem
import java.io.IOException
import kotlin.properties.Delegates
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

object FtpXmlDocumentLazyPropertyTest : Spek({
    var fakeServer: FakeFtpServer by Delegates.notNull()
    var ftpClient: FtpClient by Delegates.notNull()

    val xml = """<?xml version="1.0" encoding="UTF-8" standalone="no"?>
        |<root></root>
    """.trimMargin()

    fun createFakeServer(): FakeFtpServer {
        return FakeFtpServer().apply {
            addUserAccount(UserAccount("user", "password", "/data"))
            UnixFakeFileSystem().apply {
                add(DirectoryEntry("/data"))
                add(FileEntry("/data/xml1.xml", xml))
            }.let {
                fileSystem = it
            }
            serverControlPort = 0
            start()
        }
    }

    fun createFtpClient(fakeServer: FakeFtpServer): FtpClient {
        return FtpClient("localhost", fakeServer.serverControlPort, "user", "password").apply { load() }
    }

    describe("FtpXmlDocumentLazyProperty") {
        beforeEachTest {
            fakeServer = createFakeServer()
            ftpClient = createFtpClient(fakeServer)
        }

        afterEachTest {
            ftpClient.close()
            fakeServer.stop()
        }

        context("Construct") {
            on("trying to create document from existing sources") {
                val xmlDoc = FtpXmlDocumentLazyProperty("/data/xml1.xml", ftpClient)

                it("should create one without exceptions") {
                    xmlDoc.construct()
                }

                it("should throw exception if server closed connection session") {
                    fakeServer.setCommandHandler("RETR") { _, session ->
                        session.close()
                    }
                    assertFailsWith(FTPConnectionClosedException::class, "xmlDoc.construct()") { xmlDoc.construct() }
                }
            }

            on("trying to create document from not existing sources") {
                val xmlDoc = FtpXmlDocumentLazyProperty("/data/xml2.xml", ftpClient)
                it("should throw IOException") {
                    assertFailsWith(IOException::class, "xmlDoc.construct()") { xmlDoc.construct() }
                }
            }
        }

        context("Persist") {
            on("trying to persist document") {
                val documentFactory = DocumentFactory()
                val xmlFtpDoc = FtpXmlDocumentLazyProperty("/data/temp/properties.xml", ftpClient)
                var doc by xmlFtpDoc
                doc = documentFactory.document

                it("should execute without failures") {
                    xmlFtpDoc.tryPersist()
                }

                it("should contain the file on remote server") {
                    assertTrue("expected fakeServer.fileSystem.exists to return true, but was false") {
                        fakeServer.fileSystem.exists("/data/temp/properties.xml")
                    }
                }

                it("should not be empty") {
                    val bytesFromRemote = ftpClient.tryGetRemoteAsInputStream("/data/temp/properties.xml") {
                        it.readBytes()
                    }

                    assertTrue("expected bytesFromRemote.isNotEmpty to return true, but was false") {
                        bytesFromRemote.isNotEmpty()
                    }
                }
            }
        }
    }
})
