package com.malencur.file.xml

import com.malencur.file.xml.property.XmlPropertiesFromDocumentTest
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import javax.xml.parsers.DocumentBuilderFactory
import kotlin.test.assertNotNull

/**
 * This is just basic tests.
 * Major functionality part is tested with [XmlPropertiesFromDocumentTest]
 */
object XmlNodeUtilsTest : Spek({
    describe("XmlNodeUtils") {
        given("XmlNodeUtils") {

            val fileBuilder = object {
                fun getFile() = this.javaClass.classLoader.getResource(XML_FILE_NAME).file
            }
            val doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(fileBuilder.getFile())

            on("getNode with Document as an argument") {
                val node = getNode(doc, "terminals", "terminal")
                it("should not be null") {
                    assertNotNull(node)
                }
            }
        }
    }
})