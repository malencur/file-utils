package com.malencur.file.xml

import com.malencur.file.FileFactory
import com.malencur.file.externalDir
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.xml.sax.SAXParseException
import java.io.File
import kotlin.test.assertFailsWith

object XmlDocumentFactoryTest : Spek({
    describe("XmlDocumentFactoryTest") {
        context("broken xml") {
            FileFactory.deleteFile(externalDir + File.separator + BROKEN_XML_FILE_NAME)
            FileFactory.generateDefaultFile(
                    BROKEN_XML_FILE_NAME,
                    externalDir + File.separator + BROKEN_XML_FILE_NAME,
                    this.javaClass.classLoader
            )
            on("referencing it with exception listener") {
                it("should say that file is broken") {
                    assertFailsWith(ExpectedException::class) { BrokenDocumentWithListener() }
                }
            }
            on("referencing it without exception listener") {
                it("should fail") {
                    assertFailsWith(SAXParseException::class) { BrokenDocumentWithoutListener().document }
                }
            }
        }
    }
})