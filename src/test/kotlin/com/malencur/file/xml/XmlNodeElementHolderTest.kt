package com.malencur.file.xml

import com.malencur.file.FileFactory
import com.malencur.file.externalDir
import com.malencur.file.xml.property.XmlElementContext
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import java.io.File
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNotNull

object XmlNodeElementHolderTest : Spek({
    describe("XmlNodeElementHolder Subclass") {
        context("with existing xml nodes") {
            FileFactory.deleteFile(externalDir + File.separator + XML_FILE_NAME)
            val documentFactory = DocumentFactory()
            val terminalsHolder = documentFactory.terminalsHolder
            on("getting xml element as property") {
                it("should return it") {
                    assertNotNull(terminalsHolder)
                }
            }
            on("getting nested objects") {
                val terminals = terminalsHolder.terminals
                it("should return them") {
                    assertNotNull(terminals)
                }
                it("should return correct list size") {
                    assertEquals(5, terminals.size)
                }
                it("should return correct data") {
                    val clientAssigned = terminals[0].clientAssigned
                    assertEquals(0, clientAssigned.number)
                    val previousClients = terminals[1].previousClientsHolder.clients
                    assertEquals(3, previousClients.size)
                    assertEquals(1, previousClients[0].number)
                    assertEquals(2, previousClients[1].number)
                    assertEquals(3, previousClients[2].number)
                }
            }
            on("setting new values") {
                var terminals = documentFactory.terminalsHolder.terminals
                terminals[0].levelIndex = 20
                documentFactory.saveDocument()
                val newDocumentFactory = DocumentFactory()
                terminals = newDocumentFactory.terminalsHolder.terminals
                it("should return saved value") {
                    assertEquals(20, terminals[0].levelIndex)
                }
            }
            on("removing items from list") {
                val terminal = documentFactory.terminalsHolder.terminals[1]
                val clients = terminal.previousClientsHolder.clients
                val client = clients[0]
                clients.remove(client)

            }
            on("trying to create list item of existing type") {
                it("should not throw any error") {
                    terminalsHolder.createNewListItemChild(Terminal::class)
                }
            }
        }
        context("with none existing xml nodes") {
            FileFactory.deleteFile(externalDir + File.separator + XML_FILE_NAME)
            val documentFactory = DocumentFactory()
            val terminals = documentFactory.terminalsHolder.terminals
            on("getting list holder") {
                val previousClientsHolder = terminals[0].previousClientsHolder
                it("should create and return a new one") {
                    assertNotNull(previousClientsHolder)
                }
                it("should contain no items") {
                    assertEquals(0, previousClientsHolder.clients.size)
                }
            }
            on("populating empty list holder") {
                val previousClientsHolder = terminals[0].previousClientsHolder
                previousClientsHolder.clients.add(ClientNode(
                        XmlElementContext(documentFactory.document, previousClientsHolder.element, "client")
                ))
                documentFactory.saveDocument()
                val newDocumentFactory = DocumentFactory()
                val terminalList = newDocumentFactory.terminalsHolder.terminals

                it("should create xml nodes") {
                    assertEquals(1, terminalList[0].previousClientsHolder.clients.size)
                }
            }
            on("trying to create list item of none existing type") {
                val terminalsHolder = documentFactory.terminalsHolder

                it("should throw an error") {
                    assertFailsWith(IllegalArgumentException::class) {
                        terminalsHolder.createNewListItemChild(ClientAssigned::class)
                    }
                }
            }
        }
        context("with new document") {
            FileFactory.deleteFile(externalDir + File.separator + XML_FILE_NAME)
            val documentFactory = NewDocumentFactory()
            on("trying to create list item of existing type") {
                val terminalsHolder = documentFactory.terminalsHolder

                it("should not throw any error") {
                    val child = terminalsHolder.createNewListItemChild(Terminal::class)
                    child.visible = true
                }
            }
        }
    }
})