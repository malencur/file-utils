package com.malencur.file

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.mockftpserver.fake.FakeFtpServer
import org.mockftpserver.fake.UserAccount
import org.mockftpserver.fake.filesystem.DirectoryEntry
import org.mockftpserver.fake.filesystem.FileEntry
import org.mockftpserver.fake.filesystem.UnixFakeFileSystem
import java.io.File
import java.io.IOException
import kotlin.properties.Delegates
import kotlin.test.*

object FileFactoryTest: Spek({
    val existingFile = "test.txt"
    val loader = this.javaClass.classLoader
    val rootDir = File(loader.getResource("testdir").toURI()).absolutePath
    val subDir = "testsubdir"
    val sep = File.separator

    describe("FileFactory without FTP") {
        on("init without subDir parameter") {
            val factory = FileFactory("test", "versiya")
            it("should not be null") {
                assertNotNull(factory)
            }
        }
        on("check file existence without subDir parameter") {
            val exist = FileFactory.fileExists("$rootDir$sep$existingFile")
            it("should confirm that file exists") {
                assertTrue(exist)
            }
        }

        on("check file existence with subDir parameter") {
            val exist = FileFactory.fileExists("$rootDir$sep$subDir$sep$existingFile")
            it("should confirm that file exists") {
                assertTrue(exist)
            }
        }
        on("creating directory in users home folder"){
            val newDirPath = FileFactory.createDirInUsersHome(ROOT_DIR + File.separator + SUB_DIR)
            it("should return constructed path") {
                assertTrue(newDirPath.isNotEmpty())
            }
        }

        on("trying to generate file from template and put it into directory that not created yet") {
            val newSubDir = "$externalDir${sep}newSubDir"
            val destination = "$newSubDir$sep$existingFile"
            FileFactory.deleteFile(destination)
            FileFactory.deleteFile(newSubDir)
            val success = FileFactory.generateDefaultFile(existingFile, destination, loader)

            it("should create directory and generate file there") {
                assertTrue(success)
            }
            FileFactory.deleteFile(destination)
            FileFactory.deleteFile(newSubDir)
        }

        context("with not existing template file") {

            val noneExistingFile = "wrongFile.txt"

            on("check file existence with subDir parameter") {
                val exist = FileFactory.fileExists("$rootDir$sep$subDir$sep$noneExistingFile")
                it("should confirm that file does not exist") {
                    assertFalse(exist)
                }
            }
            on("check file existence without subDir parameter") {
                val exist = FileFactory.fileExists("$rootDir$sep$noneExistingFile")
                it("should confirm that file does not exist") {
                    assertFalse(exist)
                }
            }
            on("generating default file") {
                val destination = "$externalDir$sep$noneExistingFile"
                val generated = FileFactory.generateDefaultFile(noneExistingFile, destination, loader)
                it("should return false") {
                    assertFalse(generated)
                }
            }
        }
    }

    describe("FileFactory with FTP") {
        var fakeServer: FakeFtpServer by Delegates.notNull()
        var ftpClient: FtpClient by Delegates.notNull()

        fun createFakeServer(): FakeFtpServer {
            return FakeFtpServer().apply {
                addUserAccount(UserAccount("user", "password", "/data"))
                UnixFakeFileSystem().apply {
                    add(DirectoryEntry("/data"))
                    add(DirectoryEntry("/data/temp"))
                    add(FileEntry("/data/temp/test.txt", "abcdef 1234567890"))
                }.let {
                    fileSystem = it
                }
                serverControlPort = 0
                start()
            }
        }

        fun createFtpClient(fakeServer: FakeFtpServer): FtpClient {
            return FtpClient("localhost", fakeServer.serverControlPort, "user", "password").apply { load() }
        }
        beforeEachTest {
            fakeServer = createFakeServer()
            ftpClient = createFtpClient(fakeServer)
        }

        afterEachTest {
            ftpClient.close()
            fakeServer.stop()
        }

        on("init with not connected FTP") {
            val client = FtpClient("localhost", fakeServer.serverControlPort, "user", "password")

            it("should throw IO exception") {
                assertFailsWith(IOException::class, "FileFactory initialization") {
                    FileFactory("test.txt", "root", ftpClient = client)
                }
            }
        }

        on("init without subDir arg") {
            val factory = FileFactoryDescendant("test.txt", "/data", null, ftpClient)

            it("should build correct path") {
                assertEquals("/data/test.txt", factory.getCompleteFilePath())
            }
        }

        on("init with empty rootDir arg") {
            val factory = FileFactoryDescendant("test.txt", "", null, ftpClient)

            it("should build correct path") {
                assertEquals("/test.txt", factory.getCompleteFilePath())
            }
        }

        on("init with existing remote file") {
            val factory = FileFactoryDescendant("test.txt", "/data", "temp", ftpClient)

            it("should build correct path") {
                assertEquals("/data/temp/test.txt", factory.getCompleteFilePath())
            }

            it("should say the file exists") {
                assertTrue("expected factory.isFileExists to return true, but was false") {
                    factory.isFileExists()
                }
            }

            it("should delete existing file") {
                assertTrue("expected factory.delete to return true, but was false") {
                    factory.delete()
                }

                assertFalse("expected fakeServer.fileSystem.exists to return false, but was true") {
                    fakeServer.fileSystem.exists("/data/temp/test.txt")
                }
            }

            it("should create empty file") {
                assertTrue("expected factory.createEmpty to return true, but was false") {
                    factory.createEmpty()
                }

                assertTrue("expected fakeServer.fileSystem.exists to return true, but was false") {
                    fakeServer.fileSystem.exists("/data/temp/test.txt")
                }

                val bytesFromRemote = ftpClient.tryGetRemoteAsInputStream("/data/temp/test.txt") {
                    it.readBytes()
                }

                assertTrue("expected bytesFromRemote to be empty, but was not") { bytesFromRemote.isEmpty() }
            }

            it("should not generate default from resource") {
                assertFalse("expected factory.generateDefault to return false, but was true") {
                    factory.generateDefault()
                }

                val bytesFromRemote = ftpClient.tryGetRemoteAsInputStream("/data/temp/test.txt") {
                    it.readBytes()
                }

                assertTrue("expected bytesFromRemote to be empty, but was not") { bytesFromRemote.isEmpty() }
            }
        }

        on("init with not existing remote file") {
            val factory = FileFactoryDescendant("properties.txt", "/data", "newDir", ftpClient)

            it("should say the file doesn't exist") {
                assertFalse("expected factory.isFileExists to return false, but was true") {
                    factory.isFileExists()
                }
            }

            it("should return false on trying to  delete file") {
                assertFalse("expected factory.delete to return false, but was true") {
                    factory.delete()
                }
            }

            it("should create empty file") {
                assertTrue("expected factory.createEmpty to return true, but was false") {
                    factory.createEmpty()
                }

                assertTrue("expected fakeServer.fileSystem.exists to return true, but was false") {
                    fakeServer.fileSystem.exists("/data/newDir/properties.txt")
                }

                val bytesFromRemote = ftpClient.tryGetRemoteAsInputStream("/data/newDir/properties.txt") {
                    it.readBytes()
                }

                assertTrue("expected bytesFromRemote to be empty, but was not") { bytesFromRemote.isEmpty() }
            }
        }
    }
})

class FileFactoryDescendant(fileName: String, rootDir: String, subDir: String?, ftpClient: FtpClient?) : FileFactory(fileName, rootDir, subDir, ftpClient) {

    fun getCompleteFilePath() = destinationFilePath
    fun isFileExists() = fileExists
    fun delete() = deleteFile()
    fun createEmpty() = createEmptyFile()
    fun generateDefault() = generateDefaultFileFromResources()
}