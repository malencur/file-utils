package com.malencur.file

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import kotlin.test.assertNotNull

object AudioTest: Spek({
    describe("Audio class"){
        val audio = Audio("notify.wav"){fileName -> inputStreamFromResource(fileName)}
        on("creating new instance") {
            it("should not be null") {
                assertNotNull(audio)
            }
        }
        on("playing") {
            audio.use {
                val sourceDataLine = it.play()
                it("should return SourceDataLine") {
                    assertNotNull(sourceDataLine)
                }
            }
        }
    }
})